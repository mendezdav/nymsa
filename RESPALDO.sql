-- MySQL dump 10.13  Distrib 5.6.12, for Win32 (x86)
--
-- Host: localhost    Database: nymsa_test
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anulacion`
--

DROP TABLE IF EXISTS `anulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anulacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `factura_anulada` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anulacion`
--

LOCK TABLES `anulacion` WRITE;
/*!40000 ALTER TABLE `anulacion` DISABLE KEYS */;
INSERT INTO `anulacion` VALUES (1,'2014-01-06 11:04:02','admin',42);
/*!40000 ALTER TABLE `anulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficiario`
--

DROP TABLE IF EXISTS `beneficiario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beneficiario` (
  `cliente` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `dui` varchar(45) DEFAULT NULL,
  `afinidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficiario`
--

LOCK TABLES `beneficiario` WRITE;
/*!40000 ALTER TABLE `beneficiario` DISABLE KEYS */;
/*!40000 ALTER TABLE `beneficiario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bodega`
--

DROP TABLE IF EXISTS `bodega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bodega` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  `encargado` int(11) DEFAULT NULL,
  `descripcion` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bodega`
--

LOCK TABLES `bodega` WRITE;
/*!40000 ALTER TABLE `bodega` DISABLE KEYS */;
INSERT INTO `bodega` VALUES (1,'bodega Principal',1,'bodega principal'),(2,'bodega de Tienda',1,'bodega de tienda'),(3,'Ofertas',1,'Bodega de ofertas'),(4,'Otra',2,'Bodega de prueba');
/*!40000 ALTER TABLE `bodega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caja`
--

DROP TABLE IF EXISTS `caja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `f_factura` varchar(30) DEFAULT NULL,
  `f_exp` varchar(30) DEFAULT NULL,
  `f_despacho` varchar(30) DEFAULT NULL,
  `f_nota_credito` varchar(30) DEFAULT NULL,
  `f_nota_debito` varchar(30) DEFAULT NULL,
  `f_recibo` varchar(30) DEFAULT NULL,
  `encargado` varchar(20) NOT NULL,
  `ultimo_pedido` int(11) DEFAULT '0',
  `ultimo_cambio` int(11) DEFAULT '0',
  `bodega_por_defecto` int(11) NOT NULL,
  `serie_factura` int(11) NOT NULL,
  `serie_nota_credito` int(11) NOT NULL,
  `serie_recibo` int(11) NOT NULL,
  `serie_ticket` int(11) NOT NULL,
  `p_cambio_bodega` tinyint(1) DEFAULT '0',
  `codigo_factura` varchar(10) DEFAULT NULL,
  `codigo_nota_credito` varchar(10) DEFAULT NULL,
  `codigo_ticket` varchar(10) DEFAULT NULL,
  `codigo_recibo` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `encargado` (`encargado`),
  KEY `bodega_por_defecto` (`bodega_por_defecto`),
  KEY `serie_factura` (`serie_factura`),
  KEY `serie_nota_credito` (`serie_nota_credito`),
  KEY `serie_recibo` (`serie_recibo`),
  KEY `serie_ticket` (`serie_ticket`),
  CONSTRAINT `caja_ibfk_1` FOREIGN KEY (`encargado`) REFERENCES `empleado` (`usuario`),
  CONSTRAINT `caja_ibfk_2` FOREIGN KEY (`bodega_por_defecto`) REFERENCES `bodega` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caja`
--

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;
INSERT INTO `caja` VALUES (4,'FACTURACION 1','','','','','','','admin',2,0,1,1,0,0,0,0,'FC3','','','');
/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caja_pedido_referencia`
--

DROP TABLE IF EXISTS `caja_pedido_referencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caja_pedido_referencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caja` int(11) NOT NULL,
  `pedido` int(11) NOT NULL,
  `referencia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caja_pedido_referencia`
--

LOCK TABLES `caja_pedido_referencia` WRITE;
/*!40000 ALTER TABLE `caja_pedido_referencia` DISABLE KEYS */;
INSERT INTO `caja_pedido_referencia` VALUES (13,4,1,41),(14,4,2,42);
/*!40000 ALTER TABLE `caja_pedido_referencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogo`
--

DROP TABLE IF EXISTS `catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `inicio` date NOT NULL,
  `final` date NOT NULL,
  `nombre` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo`
--

LOCK TABLES `catalogo` WRITE;
/*!40000 ALTER TABLE `catalogo` DISABLE KEYS */;
INSERT INTO `catalogo` VALUES (10,'CampaÃ±a para aÃ±o 2013','2013-11-02','2013-12-30','CampaÃ±a 25'),(11,'AÃ±o 2004','2013-11-16','2013-11-30','CampaÃ±a 20');
/*!40000 ALTER TABLE `catalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `codigo_afiliado` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_promotor` int(11) DEFAULT NULL,
  `dui` varchar(10) DEFAULT NULL,
  `extendido_en` varchar(40) DEFAULT NULL,
  `nacionalidad` varchar(30) DEFAULT NULL,
  `primer_apellido` varchar(20) DEFAULT NULL,
  `segundo_apellido` varchar(20) DEFAULT NULL,
  `apellido_casada` varchar(20) DEFAULT NULL,
  `primer_nombre` varchar(20) DEFAULT NULL,
  `segundo_nombre` varchar(20) DEFAULT NULL,
  `telefono_celular` varchar(10) DEFAULT NULL,
  `telefono_oficina` varchar(10) DEFAULT NULL,
  `telefono_casa` varchar(10) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `correo_electronico` varchar(55) DEFAULT NULL,
  `direccion` varchar(70) DEFAULT NULL,
  `departamento` int(11) DEFAULT NULL,
  `municipio` int(11) DEFAULT NULL,
  `zona` int(11) DEFAULT NULL,
  `agente` int(11) DEFAULT NULL,
  `dias_credito` int(11) DEFAULT NULL,
  `transitorio` int(11) DEFAULT NULL,
  `genero` int(11) DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  `nit` varchar(20) DEFAULT NULL,
  `credito_fiscal` varchar(30) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `fecha_baja` date DEFAULT NULL,
  `beneficiario` varchar(70) DEFAULT NULL,
  `cedula_beneficiario` varchar(10) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `vinculo_familiar` int(11) DEFAULT NULL,
  `lugar_trabajo` varchar(85) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `direccion_trabajo` varchar(85) DEFAULT NULL,
  `telefono_celular_1` varchar(10) DEFAULT NULL,
  `telefono_celular_2` varchar(10) DEFAULT NULL,
  `extension_oficiana` varchar(8) DEFAULT NULL,
  `numero_recibo_inscripcion` int(11) DEFAULT NULL,
  `referida_por` int(11) DEFAULT NULL,
  `nombre_conyugue` varchar(45) DEFAULT NULL,
  `lugar_trabajo_conyugue` varchar(85) DEFAULT NULL,
  `cargo_conyugue` varchar(45) DEFAULT NULL,
  `direccion_trabajo_conyugue` varchar(85) DEFAULT NULL,
  `telefono_trabajo_conyugue` varchar(10) DEFAULT NULL,
  `telefono_conyugue` varchar(10) DEFAULT NULL,
  `celular_conyugue` varchar(10) DEFAULT NULL,
  `direccion_envio` varchar(85) DEFAULT NULL,
  `referido_por` int(11) DEFAULT NULL,
  `descuento` float DEFAULT '10',
  `credito` decimal(10,2) DEFAULT NULL,
  `credito_usado` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`codigo_afiliado`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,0,'1234','el salvador','salvador','mendez','parras','no tiene','David','William','77981231','22199123','22091832','1993-11-06','correo','direccion',1,2,0,4,0,0,0,0,'22981212312','12','2013-11-12','0000-00-00','beneficiario','2298173212','22123i12',0,'','','','','','',0,0,'','','','','','','','',0,10,150.00,0.00),(2,0,'0192129','San Salvador','Salvadoreño','Parras','Mendez','','William','David','7911982','2298882','2298173','1993-11-06','william.parras.mendez@gmail.com','Col. San carlos',0,0,0,0,0,0,0,0,'30091920','091123991','2013-05-06','2013-04-06','Victoria Raquel','1923109120','2201923',0,'','','','','','',0,0,'','','','','','','','',0,10,150.00,7.65),(3,0,'87109081-8','San Salvador','Salvadoreno','Hernandez','Hernandez','','Walter','Manuel','77981290','22098130','22981883','1992-05-04','walter@hotmail.com','San salvador',0,0,0,0,0,0,0,0,'9981-728371-82','9912391988','2013-05-06','2013-04-06','José Martinez','2209100293','22091883',0,'','','','','','',0,0,'','','','','','','','',0,10,150.00,0.00),(4,0,'93129312-9','San Salvador','Salvadoreño','Rivera','Romero','','Jorge','Lui','','2209-1881','2929-9109','1992-09-03','jorgeluis@hotmail.com','San Salvador, San salvador',0,0,0,0,0,0,0,0,'9282-930192-30','9210920102','2013-05-06','0000-00-00','Lisset Rivera','19209410-2','2299-1029',0,'','','','7798-0192','','',0,0,'','','','','','','','',0,10,150.00,0.00),(5,NULL,'81762530-9',NULL,NULL,'Rivera',NULL,NULL,'Samuel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2254-1983',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(6,NULL,'89071625-9',NULL,NULL,'Iveth',NULL,NULL,'Alejandra',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2251-7816',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(7,NULL,'89059817-9',NULL,NULL,'Perez',NULL,NULL,'Rodrigo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2091-3891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(8,NULL,'89057887-0',NULL,NULL,'Garcia',NULL,NULL,'Rodrigo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2289-3391',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(9,NULL,'89059009-1',NULL,NULL,'Mendez',NULL,NULL,'Jose',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2211-3495',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(10,NULL,'45561264-5',NULL,NULL,'Mendez',NULL,NULL,'Ivet',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2211-3322',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(11,NULL,'541232-5',NULL,NULL,'Mendez',NULL,NULL,'Raquel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2277-9922',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00),(12,NULL,'54123332-5',NULL,NULL,'Mendez',NULL,NULL,'Efrain',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2277-9990',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,150.00,0.00);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'negro'),(2,'cafe'),(3,'azul'),(4,'blanco'),(5,'vino'),(6,'beige'),(7,'gris'),(8,'amarillo'),(9,'bronce'),(10,'celeste'),(11,'petroleo'),(12,'dorado'),(13,'melocoton'),(14,'rosa'),(15,'fuciaavellana'),(16,'cemento'),(17,'arena'),(18,'azafran'),(19,'salmon'),(20,'corinto'),(21,'cocoa'),(22,'rojo'),(23,'rosado'),(24,'coral'),(25,'verde'),(26,'miel'),(27,'marron'),(28,'champang'),(29,'plata'),(30,'chocolate'),(31,'lila'),(32,'morado'),(33,'camel'),(34,'colores varios'),(35,'naranja'),(36,'agua'),(37,'conac'),(38,'turquesa'),(39,'kaki'),(40,'otro'),(41,'hongo');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color_producto`
--

DROP TABLE IF EXISTS `color_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color_producto` (
  `color_estilo_producto` varchar(20) NOT NULL,
  `color` int(11) NOT NULL,
  PRIMARY KEY (`color_estilo_producto`,`color`),
  KEY `fk_color_producto_1_idx` (`color`),
  KEY `fk_producto_3_idx` (`color_estilo_producto`),
  CONSTRAINT `fk_color_producto_1` FOREIGN KEY (`color`) REFERENCES `color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_3` FOREIGN KEY (`color_estilo_producto`) REFERENCES `producto` (`estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color_producto`
--

LOCK TABLES `color_producto` WRITE;
/*!40000 ALTER TABLE `color_producto` DISABLE KEYS */;
INSERT INTO `color_producto` VALUES ('101',1),('102',1),('101',2),('102',3);
/*!40000 ALTER TABLE `color_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_precio`
--

DROP TABLE IF EXISTS `control_precio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_precio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `control_estilo` varchar(20) NOT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `costo` float DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `control_estilo` (`control_estilo`),
  CONSTRAINT `control_precio_ibfk_1` FOREIGN KEY (`control_estilo`) REFERENCES `producto` (`estilo`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_precio`
--

LOCK TABLES `control_precio` WRITE;
/*!40000 ALTER TABLE `control_precio` DISABLE KEYS */;
INSERT INTO `control_precio` VALUES (2,'101',1,11.00,1,3.00,5.5),(3,'101',1,13.00,2,10.00,7.65),(5,'101',1,10.00,1,3.00,17),(6,'101',1,12.00,1,3.00,16.34),(7,'102',2,16.00,2,55.00,12.99),(8,'101',1,14.00,2,10.00,22.56);
/*!40000 ALTER TABLE `control_precio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_precio_documento`
--

DROP TABLE IF EXISTS `control_precio_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_precio_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `control_estilo` varchar(20) DEFAULT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `documento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `control_estilo` (`control_estilo`),
  KEY `fk_documento_precio` (`documento`),
  CONSTRAINT `control_precio_documento_ibfk_1` FOREIGN KEY (`control_estilo`) REFERENCES `producto` (`estilo`),
  CONSTRAINT `fk_documento_precio` FOREIGN KEY (`documento`) REFERENCES `documento` (`id_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_precio_documento`
--

LOCK TABLES `control_precio_documento` WRITE;
/*!40000 ALTER TABLE `control_precio_documento` DISABLE KEYS */;
INSERT INTO `control_precio_documento` VALUES (1,'101',1,11.00,1,13.00,18),(2,'101',1,13.00,2,44.00,18),(3,'101',1,10.00,1,11.00,18),(4,'101',1,12.00,1,100.00,18),(5,'102',2,16.00,2,55.00,18),(6,'101',1,14.00,2,15.64,18);
/*!40000 ALTER TABLE `control_precio_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_factura`
--

DROP TABLE IF EXISTS `detalle_factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_factura` (
  `descripcion` varchar(200) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `porcentaje` float DEFAULT '0',
  `descuento` float DEFAULT '0',
  `importe` decimal(10,2) NOT NULL,
  `id_factura` int(11) NOT NULL,
  `bodega` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linea` int(11) NOT NULL,
  `estilo` varchar(20) NOT NULL,
  `color` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_factura` (`id_factura`),
  CONSTRAINT `detalle_factura_ibfk_1` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`id_factura`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_factura`
--

LOCK TABLES `detalle_factura` WRITE;
/*!40000 ALTER TABLE `detalle_factura` DISABLE KEYS */;
INSERT INTO `detalle_factura` VALUES ('1-101-1-11',3.00,2,10,-0.6,6.00,41,0,1,1,'101',1,11.00),('1-101-1-11.00',3.00,4,10,-1.2,12.00,42,1,2,1,'101',1,11.00),('1-101-2-14.00',10.00,1,10,-1,10.00,42,1,3,1,'101',2,14.00);
/*!40000 ALTER TABLE `detalle_factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_orden_compra`
--

DROP TABLE IF EXISTS `detalle_orden_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_orden_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) NOT NULL,
  `linea` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  `color` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `costo` decimal(10,2) DEFAULT NULL,
  `id_orden` int(11) NOT NULL,
  `recibidos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_orden` (`id_orden`),
  CONSTRAINT `detalle_orden_compra_ibfk_1` FOREIGN KEY (`id_orden`) REFERENCES `orden_compra` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_orden_compra`
--

LOCK TABLES `detalle_orden_compra` WRITE;
/*!40000 ALTER TABLE `detalle_orden_compra` DISABLE KEYS */;
INSERT INTO `detalle_orden_compra` VALUES (20,'101',1,11.00,1,10,5.50,70,10),(21,'102',2,16.00,2,4,12.99,70,0);
/*!40000 ALTER TABLE `detalle_orden_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_remision`
--

DROP TABLE IF EXISTS `detalle_remision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_remision` (
  `descripcion` varchar(200) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `porcentaje` float DEFAULT '0',
  `descuento` float DEFAULT '0',
  `importe` decimal(10,2) NOT NULL,
  `id_remision` int(11) NOT NULL,
  KEY `id_remision` (`id_remision`),
  CONSTRAINT `detalle_remision_ibfk_1` FOREIGN KEY (`id_remision`) REFERENCES `remision` (`id_remision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_remision`
--

LOCK TABLES `detalle_remision` WRITE;
/*!40000 ALTER TABLE `detalle_remision` DISABLE KEYS */;
INSERT INTO `detalle_remision` VALUES ('1-101-1-10.00',3.00,1,10,-0.3,3.00,24);
/*!40000 ALTER TABLE `detalle_remision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_traslado`
--

DROP TABLE IF EXISTS `detalle_traslado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_traslado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ref` int(11) NOT NULL,
  `linea` int(11) NOT NULL,
  `estilo` varchar(20) NOT NULL,
  `color` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  `costo` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `bodega` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_ref` (`id_ref`),
  CONSTRAINT `detalle_traslado_ibfk_1` FOREIGN KEY (`id_ref`) REFERENCES `traslado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_traslado`
--

LOCK TABLES `detalle_traslado` WRITE;
/*!40000 ALTER TABLE `detalle_traslado` DISABLE KEYS */;
INSERT INTO `detalle_traslado` VALUES (31,19,1,'101',2,13.00,7.65,1,7.65,0);
/*!40000 ALTER TABLE `detalle_traslado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devolucion`
--

DROP TABLE IF EXISTS `devolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) DEFAULT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `bodega` int(11) DEFAULT NULL,
  `cliente` int(11) NOT NULL,
  `factura` int(11) DEFAULT NULL,
  `devueltos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`),
  CONSTRAINT `devolucion_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo_afiliado`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devolucion`
--

LOCK TABLES `devolucion` WRITE;
/*!40000 ALTER TABLE `devolucion` DISABLE KEYS */;
INSERT INTO `devolucion` VALUES (1,'101',1,11.00,1,1,1,2,5,0);
/*!40000 ALTER TABLE `devolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `id_documento` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` date NOT NULL,
  `propietario` varchar(20) NOT NULL,
  `estado` int(11) NOT NULL,
  `modulo` char(20) DEFAULT NULL,
  PRIMARY KEY (`id_documento`),
  KEY `fk_empleado_pk1_idx` (`propietario`),
  CONSTRAINT `fk_empleado_pk1` FOREIGN KEY (`propietario`) REFERENCES `empleado` (`usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
INSERT INTO `documento` VALUES (12,'2013-08-24','admin',1,'inventario'),(13,'2013-09-21','admin',1,'stock'),(14,'2013-10-10','admin',1,'stock'),(15,'2013-10-17','admin',1,'inventario'),(16,'2013-10-17','admin',0,'inventario'),(17,'2013-10-19','admin',1,'stock'),(18,'2013-10-25','admin',0,'stock');
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento_color_producto`
--

DROP TABLE IF EXISTS `documento_color_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento_color_producto` (
  `color_estilo_producto` varchar(20) NOT NULL,
  `color` int(11) NOT NULL,
  PRIMARY KEY (`color_estilo_producto`,`color`),
  KEY `fk_color_producto_1_idx` (`color`),
  CONSTRAINT `fk_color_producto_10` FOREIGN KEY (`color`) REFERENCES `color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_30` FOREIGN KEY (`color_estilo_producto`) REFERENCES `documento_producto` (`estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento_color_producto`
--

LOCK TABLES `documento_color_producto` WRITE;
/*!40000 ALTER TABLE `documento_color_producto` DISABLE KEYS */;
INSERT INTO `documento_color_producto` VALUES ('104',8),('104',9);
/*!40000 ALTER TABLE `documento_color_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento_producto`
--

DROP TABLE IF EXISTS `documento_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento_producto` (
  `estilo` varchar(20) NOT NULL,
  `linea` int(11) DEFAULT NULL,
  `codigo_origen` varchar(45) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `proveedor` int(11) DEFAULT NULL,
  `catalogo` int(11) DEFAULT NULL,
  `n_pagina` int(11) DEFAULT NULL,
  `genero` int(11) DEFAULT NULL,
  `marca` int(11) DEFAULT NULL,
  `propiedad` int(11) DEFAULT NULL,
  `observacion` varchar(45) DEFAULT NULL,
  `fecha_ingreso` date NOT NULL,
  `nota` varchar(45) DEFAULT NULL,
  `numero_documento` int(11) NOT NULL,
  `visible` int(11) DEFAULT NULL,
  PRIMARY KEY (`estilo`),
  KEY `fk_proveedor_1_idx` (`proveedor`),
  KEY `fk_marca_1_idx` (`marca`),
  KEY `fk_genero_1_idx` (`genero`),
  KEY `fk_linea_1_idx` (`linea`),
  KEY `fk_documento_pk1_idx` (`numero_documento`),
  CONSTRAINT `fk_documento_pk1` FOREIGN KEY (`numero_documento`) REFERENCES `documento` (`id_documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_genero_10` FOREIGN KEY (`genero`) REFERENCES `genero` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_linea_10` FOREIGN KEY (`linea`) REFERENCES `linea` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_marca_10` FOREIGN KEY (`marca`) REFERENCES `marca` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedor_11` FOREIGN KEY (`proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento_producto`
--

LOCK TABLES `documento_producto` WRITE;
/*!40000 ALTER TABLE `documento_producto` DISABLE KEYS */;
INSERT INTO `documento_producto` VALUES ('104',2,'1004','Zapatos para caballero',1,11,13,2,1,1,'','2013-06-05','',16,0);
/*!40000 ALTER TABLE `documento_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento_talla_producto`
--

DROP TABLE IF EXISTS `documento_talla_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento_talla_producto` (
  `talla_estilo_producto` varchar(20) NOT NULL,
  `color` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  PRIMARY KEY (`talla_estilo_producto`,`color`,`talla`),
  KEY `fk_color_prod1_idx` (`color`),
  KEY `fk_estilo_prod2_idx` (`talla_estilo_producto`),
  CONSTRAINT `fk_color_prod10` FOREIGN KEY (`color`) REFERENCES `documento_color_producto` (`color`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_estilo_prod20` FOREIGN KEY (`talla_estilo_producto`) REFERENCES `documento_color_producto` (`color_estilo_producto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento_talla_producto`
--

LOCK TABLES `documento_talla_producto` WRITE;
/*!40000 ALTER TABLE `documento_talla_producto` DISABLE KEYS */;
INSERT INTO `documento_talla_producto` VALUES ('104',8,5.00),('104',8,6.00),('104',8,7.00),('104',8,8.00),('104',8,9.00),('104',8,10.00),('104',9,12.00),('104',9,13.00),('104',9,14.00),('104',9,15.00),('104',9,16.00);
/*!40000 ALTER TABLE `documento_talla_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `id_datos` int(11) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `departamento` varchar(65) DEFAULT NULL,
  `descuento` decimal(10,2) DEFAULT NULL,
  `permiso` int(11) DEFAULT '0',
  `acceso` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`usuario`),
  KEY `fk_datos_` (`id_datos`),
  CONSTRAINT `fk_datos_` FOREIGN KEY (`id_datos`) REFERENCES `cliente` (`codigo_afiliado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'admin','SUE8a7vKyVA7amjZ8RwsnY98d31dfexjpKR8NQ783Mc=','administracion',10.00,1,'YTozOntzOjEwOiJpbnZlbnRhcmlvIjthOjI6e3M6NToiYWNjZXMiO2I6MTtzOjExOiJzdWJDYXRlZ29yeSI7YTo4OntzOjc6ImNvbG9yZXMiO2I6MTtzOjY6ImxpbmVhcyI7YjoxO3M6NjoibWFyY2FzIjtiOjE7czo3OiJnZW5lcm9zIjtiOjE7czo5OiJwcm9kdWN0b3MiO2I6MTtzOjExOiJwcm92ZWVkb3JlcyI7YjoxO3M6NzoiYm9kZWdhcyI7YjoxO3M6NToic3RvY2siO2I6MTt9fXM6NzoiZmFjdHVyYSI7YToyOntzOjU6ImFjY2VzIjtiOjE7czoxMToic3ViQ2F0ZWdvcnkiO2E6MTp7czoxNToiQ3JlYXIlMjBGYWN0dXJhIjtiOjE7fX1zOjc6ImNsaWVudGUiO2E6Mjp7czo1OiJhY2NlcyI7YjoxO3M6MTE6InN1YkNhdGVnb3J5IjthOjM6e3M6MTI6ImZpY2hhQ2xpZW50ZSI7YjoxO3M6MTQ6Imxpc3RhZG9DbGllbnRlIjtiOjE7czoxNToibGlzdGFkb0VtcGxlYWRvIjtiOjE7fX19'),(2,'empleado02','iigDJJfyEKDF76LQOFn9Cz5LvxYDpwRGeFJDFG+yhTA=','contabilidad',35.00,0,'YTozOntzOjEwOiJpbnZlbnRhcmlvIjthOjI6e3M6NToiYWNjZXMiO2I6MTtzOjExOiJzdWJDYXRlZ29yeSI7YTo4OntzOjc6ImNvbG9yZXMiO2I6MDtzOjY6ImxpbmVhcyI7YjowO3M6NjoibWFyY2FzIjtiOjE7czo3OiJnZW5lcm9zIjtiOjA7czo5OiJwcm9kdWN0b3MiO2I6MDtzOjExOiJwcm92ZWVkb3JlcyI7YjowO3M6NzoiYm9kZWdhcyI7YjowO3M6NToic3RvY2siO2I6MDt9fXM6NzoiZmFjdHVyYSI7YToyOntzOjU6ImFjY2VzIjtiOjA7czoxMToic3ViQ2F0ZWdvcnkiO2E6MDp7fX1zOjc6ImNsaWVudGUiO2E6Mjp7czo1OiJhY2NlcyI7YjowO3M6MTE6InN1YkNhdGVnb3J5IjthOjM6e3M6MTI6ImZpY2hhQ2xpZW50ZSI7YjoxO3M6MTQ6Imxpc3RhZG9DbGllbnRlIjtiOjA7czoxNToibGlzdGFkb0VtcGxlYWRvIjtiOjE7fX19');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrada_pendiente`
--

DROP TABLE IF EXISTS `entrada_pendiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrada_pendiente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) DEFAULT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `bodega` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_producto_3_idxsa` (`estilo`),
  KEY `fk_bodega_destino_idxsa` (`bodega`),
  CONSTRAINT `fk_bodega_destinosa` FOREIGN KEY (`bodega`) REFERENCES `bodega` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_3psa` FOREIGN KEY (`estilo`) REFERENCES `producto` (`estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrada_pendiente`
--

LOCK TABLES `entrada_pendiente` WRITE;
/*!40000 ALTER TABLE `entrada_pendiente` DISABLE KEYS */;
INSERT INTO `entrada_pendiente` VALUES (1,'101',1,11.00,1,2,1);
/*!40000 ALTER TABLE `entrada_pendiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_bodega`
--

DROP TABLE IF EXISTS `estado_bodega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_bodega` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) DEFAULT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `bodega` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_producto_3_idx` (`estilo`),
  KEY `fk_bodega_destino_idx` (`bodega`),
  CONSTRAINT `fk_bodega_destino` FOREIGN KEY (`bodega`) REFERENCES `bodega` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_3p` FOREIGN KEY (`estilo`) REFERENCES `producto` (`estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_bodega`
--

LOCK TABLES `estado_bodega` WRITE;
/*!40000 ALTER TABLE `estado_bodega` DISABLE KEYS */;
INSERT INTO `estado_bodega` VALUES (1,'101',1,12.00,1,9,1),(2,'101',1,11.00,1,132,1),(3,'101',1,13.00,2,7,1),(4,'102',2,16.00,2,17,1),(5,'101',1,14.00,2,1,1),(12,'101',1,10.00,1,20,1),(13,'101',1,11.00,1,25,4),(14,'101',1,10.00,1,6,4),(15,'101',1,13.00,2,1,4);
/*!40000 ALTER TABLE `estado_bodega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_bodega_documento`
--

DROP TABLE IF EXISTS `estado_bodega_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_bodega_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) NOT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `bodega` int(11) DEFAULT NULL,
  `documento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estilo` (`estilo`),
  KEY `bodega` (`bodega`),
  KEY `fk_documento_bodega` (`documento`),
  CONSTRAINT `estado_bodega_documento_ibfk_1` FOREIGN KEY (`estilo`) REFERENCES `producto` (`estilo`),
  CONSTRAINT `estado_bodega_documento_ibfk_2` FOREIGN KEY (`bodega`) REFERENCES `bodega` (`id`),
  CONSTRAINT `fk_documento_bodega` FOREIGN KEY (`documento`) REFERENCES `documento` (`id_documento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_bodega_documento`
--

LOCK TABLES `estado_bodega_documento` WRITE;
/*!40000 ALTER TABLE `estado_bodega_documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_bodega_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `excedente_orden_compra`
--

DROP TABLE IF EXISTS `excedente_orden_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `excedente_orden_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) NOT NULL,
  `linea` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `costo` decimal(10,2) NOT NULL,
  `id_orden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_orden` (`id_orden`),
  CONSTRAINT `excedente_orden_compra_ibfk_1` FOREIGN KEY (`id_orden`) REFERENCES `orden_compra` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `excedente_orden_compra`
--

LOCK TABLES `excedente_orden_compra` WRITE;
/*!40000 ALTER TABLE `excedente_orden_compra` DISABLE KEYS */;
INSERT INTO `excedente_orden_compra` VALUES (1,'101',1,1,11.00,-4,5.50,70),(2,'101',1,1,11.00,-4,5.50,70),(3,'101',1,1,11.00,20,5.50,70),(4,'101',1,1,11.00,-9,5.50,70),(5,'101',1,1,11.00,-9,5.50,70),(6,'101',1,1,11.00,-9,5.50,70),(7,'101',1,1,11.00,-9,5.50,70),(8,'101',1,1,11.00,290,5.50,70),(9,'101',1,1,11.00,290,5.50,70),(10,'101',1,1,11.00,-9,5.50,70),(11,'101',1,1,11.00,-9,5.50,70),(12,'101',1,1,11.00,-9,5.50,70),(13,'101',1,1,11.00,-9,5.50,70),(14,'101',1,1,11.00,-7,5.50,70),(15,'101',1,1,11.00,-9,5.50,70),(16,'101',1,1,11.00,-9,5.50,70),(17,'101',1,1,11.00,-9,5.50,70),(18,'101',1,1,11.00,-8,5.50,70),(19,'101',1,1,11.00,-8,5.50,70),(20,'101',1,1,11.00,-7,5.50,70),(21,'101',1,1,11.00,-9,5.50,70),(22,'101',1,1,11.00,-9,5.50,70),(23,'101',1,1,11.00,-8,5.50,70),(24,'101',1,1,11.00,-9,5.50,70),(25,'101',1,1,11.00,-9,5.50,70),(26,'101',1,1,11.00,-9,5.50,70),(27,'101',1,1,11.00,-9,5.50,70),(28,'101',1,1,11.00,-7,5.50,70),(29,'101',1,1,11.00,-9,5.50,70),(30,'101',1,1,11.00,-8,5.50,70),(31,'101',1,1,11.00,-7,5.50,70),(32,'101',1,1,11.00,-9,5.50,70),(33,'101',1,1,11.00,-9,5.50,70),(34,'101',1,1,11.00,-9,5.50,70),(35,'101',1,1,11.00,-9,5.50,70),(36,'101',1,1,11.00,-9,5.50,70);
/*!40000 ALTER TABLE `excedente_orden_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `descuento` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estado` varchar(25) NOT NULL DEFAULT 'PENDIENTE',
  `tipo` varchar(25) NOT NULL DEFAULT '',
  `flete` tinyint(1) DEFAULT '0',
  `caja` int(11) NOT NULL,
  `periodo_actual` varchar(10) NOT NULL,
  `modificable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_factura`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`codigo_afiliado`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (41,2,'2014-01-02',6.00,-0.60,5.40,'FACTURADO','CONTADO',0,4,'1312',0),(42,2,'2014-01-06',25.00,-2.50,22.50,'ANULADO','CONTADO',0,4,'1312',0);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiador`
--

DROP TABLE IF EXISTS `fiador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiador` (
  `cliente` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_vivienda` int(11) DEFAULT NULL,
  `tiempo_de_residir` varchar(45) DEFAULT NULL,
  `pago_mensual` varchar(45) DEFAULT NULL,
  `nombre_empresa` varchar(45) DEFAULT NULL,
  `actividad_economica` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `cargo_ejercido` varchar(45) DEFAULT NULL,
  `tiempo_de_laborar` varchar(45) DEFAULT NULL,
  `telefono_empresa` varchar(45) DEFAULT NULL,
  `jefe_inmediato` varchar(45) DEFAULT NULL,
  `sueldo_mensual` varchar(45) DEFAULT NULL,
  `otros_ingresos` decimal(10,2) DEFAULT NULL,
  `primer_nombre` varchar(45) DEFAULT NULL,
  `segundo_nombre` varchar(45) DEFAULT NULL,
  `primer_apellido` varchar(45) DEFAULT NULL,
  `segudo_apellido` varchar(45) DEFAULT NULL,
  `apellido_casada` varchar(45) DEFAULT NULL,
  `estado_civil` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular_1` varchar(45) DEFAULT NULL,
  `celular_2` varchar(45) DEFAULT NULL,
  `dui` varchar(45) DEFAULT NULL,
  `extendido_en` varchar(45) DEFAULT NULL,
  `nit` varchar(45) DEFAULT NULL,
  `es_negocio_propio` varchar(45) DEFAULT NULL,
  `matricula_contribuyente` varchar(45) DEFAULT NULL,
  `nrc` varchar(45) DEFAULT NULL,
  KEY `fk_cliente_1` (`cliente`),
  CONSTRAINT `fk_cliente_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo_afiliado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiador`
--

LOCK TABLES `fiador` WRITE;
/*!40000 ALTER TABLE `fiador` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flete`
--

DROP TABLE IF EXISTS `flete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flete` (
  `precio` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flete`
--

LOCK TABLES `flete` WRITE;
/*!40000 ALTER TABLE `flete` DISABLE KEYS */;
INSERT INTO `flete` VALUES (10);
/*!40000 ALTER TABLE `flete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,'dama'),(2,'caballero'),(3,'nina'),(4,'nino'),(5,'unisex'),(6,'no definido');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial`
--

DROP TABLE IF EXISTS `historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `fecha_hora` datetime DEFAULT NULL,
  `modulo` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial`
--

LOCK TABLES `historial` WRITE;
/*!40000 ALTER TABLE `historial` DISABLE KEYS */;
INSERT INTO `historial` VALUES (69,'admin','0','2013-10-18 07:10:07','stock'),(70,'admin','101','2013-10-18 07:10:39','stock'),(71,'admin','se agregaron  unidades al producto ','2013-10-18 10:10:35','stock'),(72,'admin','se agregaron 8 unidades al producto 101','2013-10-18 10:10:06','stock'),(73,'admin','se agregaron 9 unidades al producto 101','2013-10-19 08:10:43','stock'),(74,'admin','se agregaron 12 unidades al producto 101','2013-10-19 08:10:14','stock');
/*!40000 ALTER TABLE `historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `id_facturas`
--

DROP TABLE IF EXISTS `id_facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_facturas` (
  `id` int(11) NOT NULL,
  `serie` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  KEY `id_facturas_ibfk_1` (`id_pedido`),
  CONSTRAINT `id_facturas_ibfk_1` FOREIGN KEY (`id_pedido`) REFERENCES `factura` (`id_factura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `id_facturas`
--

LOCK TABLES `id_facturas` WRITE;
/*!40000 ALTER TABLE `id_facturas` DISABLE KEYS */;
INSERT INTO `id_facturas` VALUES (4,1,41),(5,1,42);
/*!40000 ALTER TABLE `id_facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kardex`
--

DROP TABLE IF EXISTS `kardex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kardex` (
  `fecha` date NOT NULL,
  `fecha_transaccion` date NOT NULL,
  `detalle` varchar(400) NOT NULL,
  `cantidad_en` decimal(10,2) DEFAULT NULL,
  `valor_unitario_en` decimal(10,2) DEFAULT NULL,
  `valor_total_en` decimal(10,2) DEFAULT NULL,
  `cantidad_sa` decimal(10,2) DEFAULT NULL,
  `valor_unitario_sa` decimal(10,2) DEFAULT NULL,
  `valor_total_sa` decimal(10,2) DEFAULT NULL,
  `cantidad_ex` decimal(10,2) DEFAULT NULL,
  `valor_unitario_ex` decimal(10,2) DEFAULT NULL,
  `valor_total_ex` decimal(10,2) DEFAULT NULL,
  `codigo` varchar(50) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kardex`
--

LOCK TABLES `kardex` WRITE;
/*!40000 ALTER TABLE `kardex` DISABLE KEYS */;
/*!40000 ALTER TABLE `kardex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea`
--

DROP TABLE IF EXISTS `linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea`
--

LOCK TABLES `linea` WRITE;
/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
INSERT INTO `linea` VALUES (1,'NIÃ‘OS'),(2,'CABALLERO'),(3,'DAMA');
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'JAGUAR');
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta`
--

DROP TABLE IF EXISTS `oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(140) DEFAULT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL,
  `estado` int(11) DEFAULT '0',
  `descuento` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta`
--

LOCK TABLES `oferta` WRITE;
/*!40000 ALTER TABLE `oferta` DISABLE KEYS */;
INSERT INTO `oferta` VALUES (1,'10% DESCUENTO','DESCUENTO ZAPATOS','2013-11-01','2014-12-12',0,0.10),(2,'20% DE DESCUENTO','DESCUENTO PARA DAMAS','2013-11-14','2013-11-30',0,0.30);
/*!40000 ALTER TABLE `oferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta_producto`
--

DROP TABLE IF EXISTS `oferta_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_oferta` int(11) NOT NULL,
  `estilo` varchar(20) NOT NULL,
  `linea` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_oferta` (`id_oferta`),
  KEY `estilo` (`estilo`),
  CONSTRAINT `oferta_producto_ibfk_1` FOREIGN KEY (`id_oferta`) REFERENCES `oferta` (`id`),
  CONSTRAINT `oferta_producto_ibfk_2` FOREIGN KEY (`estilo`) REFERENCES `producto` (`estilo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta_producto`
--

LOCK TABLES `oferta_producto` WRITE;
/*!40000 ALTER TABLE `oferta_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `oferta_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orden_compra`
--

DROP TABLE IF EXISTS `orden_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orden_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` char(15) NOT NULL DEFAULT 'PENDIENTE',
  `tipo` char(15) NOT NULL,
  `fecha` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `proveedor` int(11) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `total_costo` decimal(10,2) NOT NULL DEFAULT '0.00',
  `editable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `proveedor` (`proveedor`),
  CONSTRAINT `orden_compra_ibfk_1` FOREIGN KEY (`proveedor`) REFERENCES `proveedor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orden_compra`
--

LOCK TABLES `orden_compra` WRITE;
/*!40000 ALTER TABLE `orden_compra` DISABLE KEYS */;
INSERT INTO `orden_compra` VALUES (70,'ENTREGADO','inicial','0000-00-00','0000-00-00',1,14,106.96,0);
/*!40000 ALTER TABLE `orden_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `estilo` varchar(20) NOT NULL,
  `linea` int(11) DEFAULT NULL,
  `codigo_origen` varchar(45) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `proveedor` int(11) DEFAULT NULL,
  `catalogo` int(11) DEFAULT NULL,
  `n_pagina` int(11) DEFAULT NULL,
  `genero` int(11) DEFAULT NULL COMMENT '\n',
  `marca` int(11) DEFAULT NULL,
  `propiedad` int(11) DEFAULT NULL,
  `observacion` varchar(45) DEFAULT NULL,
  `fecha_ingreso` date NOT NULL,
  `nota` varchar(45) DEFAULT NULL,
  `garantia` char(1) DEFAULT 'N',
  `tacon` varchar(20) DEFAULT NULL,
  `suela` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`estilo`),
  KEY `fk_proveedor_1_idx` (`proveedor`),
  KEY `fk_marca_1_idx` (`marca`),
  KEY `fk_genero_1_idx` (`genero`),
  KEY `fk_linea_1_idx` (`linea`),
  CONSTRAINT `fk_genero_1` FOREIGN KEY (`genero`) REFERENCES `genero` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_linea_1` FOREIGN KEY (`linea`) REFERENCES `linea` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_marca_1` FOREIGN KEY (`marca`) REFERENCES `marca` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedor_1` FOREIGN KEY (`proveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES ('101',1,'1001','Zapato',1,11,12,1,1,1,'sin observacion','2013-06-05','sin nota','','',''),('102',2,'1002','Bolso',1,11,56,1,1,1,'sin observacion','2013-06-04','sin nota','N',NULL,NULL);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `fax` varchar(40) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `nombre_contacto` varchar(45) DEFAULT NULL,
  `telefono_contacto` varchar(25) DEFAULT NULL,
  `correo_contacto` varchar(45) DEFAULT NULL,
  `dias_credito` int(11) DEFAULT NULL,
  `dias_entrega` int(11) DEFAULT NULL,
  `pais_origen` varchar(40) DEFAULT NULL,
  `nacionalidad` varchar(35) DEFAULT NULL,
  `nit` varchar(25) DEFAULT NULL,
  `registro` varchar(25) DEFAULT NULL,
  `margen_usual` float DEFAULT NULL,
  `descuento_aplicable` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'ZapaterÃ­a salvadoreÃ±a','Col Escalon','22889172','2298172','casadcv@gmail.com','Carlos Jose','22231872','joscarlos@hotmaill.com',30,60,'El Salvador','SalvadoreÃ±o','88715283','71672981',30,15),(2,'Moda 20011','Col. Escalon, San Salvador','22091193','22009102','moda2001@hotmail.com','Carlos Alfaro','22099912','carlos@gmail.com',30,60,'El Salvador','Salvadoreño','2209193984','91092319209',50,10);
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referencias`
--

DROP TABLE IF EXISTS `referencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referencias` (
  `cliente` int(10) unsigned NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `telefono_1` varchar(45) DEFAULT NULL,
  `afinidad` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `movil_1` varchar(45) DEFAULT NULL,
  `movil_2` varchar(45) DEFAULT NULL,
  `referencia_credito` varchar(45) DEFAULT NULL,
  `tipo_referencia` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referencias`
--

LOCK TABLES `referencias` WRITE;
/*!40000 ALTER TABLE `referencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `referencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referencias_crediticias`
--

DROP TABLE IF EXISTS `referencias_crediticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referencias_crediticias` (
  `cliente` int(11) NOT NULL,
  `nombre_empresa` varchar(45) NOT NULL,
  `monto_otorgado` decimal(10,2) NOT NULL,
  PRIMARY KEY (`cliente`,`nombre_empresa`,`monto_otorgado`),
  KEY `fk_cliente_idx` (`cliente`),
  CONSTRAINT `fk_cliente` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo_afiliado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referencias_crediticias`
--

LOCK TABLES `referencias_crediticias` WRITE;
/*!40000 ALTER TABLE `referencias_crediticias` DISABLE KEYS */;
/*!40000 ALTER TABLE `referencias_crediticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referencias_crediticias_fiador`
--

DROP TABLE IF EXISTS `referencias_crediticias_fiador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referencias_crediticias_fiador` (
  `cliente` int(11) NOT NULL,
  `nombre_empresa` varchar(45) NOT NULL,
  `monto_otorgado` varchar(45) NOT NULL,
  PRIMARY KEY (`cliente`,`nombre_empresa`,`monto_otorgado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referencias_crediticias_fiador`
--

LOCK TABLES `referencias_crediticias_fiador` WRITE;
/*!40000 ALTER TABLE `referencias_crediticias_fiador` DISABLE KEYS */;
/*!40000 ALTER TABLE `referencias_crediticias_fiador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referencias_fiador`
--

DROP TABLE IF EXISTS `referencias_fiador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referencias_fiador` (
  `cliente` int(10) unsigned NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `telefono_1` varchar(45) DEFAULT NULL,
  `afinidad` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `movil_1` varchar(45) DEFAULT NULL,
  `movil_2` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referencias_fiador`
--

LOCK TABLES `referencias_fiador` WRITE;
/*!40000 ALTER TABLE `referencias_fiador` DISABLE KEYS */;
/*!40000 ALTER TABLE `referencias_fiador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remision`
--

DROP TABLE IF EXISTS `remision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remision` (
  `id_remision` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_remision`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `remision_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`codigo_afiliado`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remision`
--

LOCK TABLES `remision` WRITE;
/*!40000 ALTER TABLE `remision` DISABLE KEYS */;
INSERT INTO `remision` VALUES (18,NULL,'2013-12-18',0),(19,NULL,'2013-12-18',0),(20,NULL,'2013-12-18',0),(21,NULL,'2013-12-18',0),(22,NULL,'2013-12-18',0),(23,NULL,'2013-12-18',0),(24,2,'2013-12-18',0),(25,NULL,'2013-12-18',0);
/*!40000 ALTER TABLE `remision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserva_numero`
--

DROP TABLE IF EXISTS `reserva_numero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserva_numero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_factura_reservada` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva_numero`
--

LOCK TABLES `reserva_numero` WRITE;
/*!40000 ALTER TABLE `reserva_numero` DISABLE KEYS */;
INSERT INTO `reserva_numero` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),(23,23),(24,24),(25,25),(26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32),(33,33),(34,34),(35,35),(36,36),(37,37),(38,38),(39,39),(40,40),(41,41),(42,42),(43,43),(44,44),(45,45),(46,46),(47,47),(48,48),(49,49),(50,50),(51,51),(52,52),(53,53),(54,54),(55,55),(56,56),(57,57),(58,58),(59,59),(60,60),(61,61),(62,62),(63,63),(64,64),(65,65),(66,66),(67,67),(68,68),(69,69),(70,70),(71,71),(72,72),(73,73),(74,74),(75,75),(76,76),(77,77),(78,78),(79,79),(80,80),(81,81),(82,82),(83,83),(84,84),(85,85),(86,86),(87,87),(88,88),(89,89),(90,90),(91,91),(92,92),(93,93),(94,94),(95,95),(96,96),(97,97),(98,98),(99,99),(100,100),(101,1),(102,2),(103,3),(104,4),(105,16);
/*!40000 ALTER TABLE `reserva_numero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserva_remision`
--

DROP TABLE IF EXISTS `reserva_remision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserva_remision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_remision_reservada` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva_remision`
--

LOCK TABLES `reserva_remision` WRITE;
/*!40000 ALTER TABLE `reserva_remision` DISABLE KEYS */;
INSERT INTO `reserva_remision` VALUES (1,3),(2,4),(3,5),(4,6),(5,7),(6,8),(7,9),(8,10),(9,11),(10,12),(11,13),(12,14),(13,15),(14,16),(15,17),(16,1),(17,19),(18,20),(19,21),(20,22),(21,23),(22,24),(23,25);
/*!40000 ALTER TABLE `reserva_remision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serie`
--

DROP TABLE IF EXISTS `serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serie` (
  `tipo` char(4) NOT NULL,
  `serie` varchar(10) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `resolucion` varchar(15) DEFAULT NULL,
  `fecha_resolucion` date DEFAULT NULL,
  `del` int(11) DEFAULT '0',
  `al` int(11) DEFAULT '999999',
  `ultimo_utilizado` int(11) DEFAULT '0',
  `lineas_doc` int(11) DEFAULT '99',
  `doc_caja` tinyint(1) DEFAULT '0',
  `afecta_iva` tinyint(1) DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serie`
--

LOCK TABLES `serie` WRITE;
/*!40000 ALTER TABLE `serie` DISABLE KEYS */;
INSERT INTO `serie` VALUES ('FC','FC3','13DS000F','','0000-00-00',0,40000,5,99,0,0,1);
/*!40000 ALTER TABLE `serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) DEFAULT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_producto_1_idx` (`estilo`),
  CONSTRAINT `fk_producto_1` FOREIGN KEY (`estilo`) REFERENCES `producto` (`estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system`
--

DROP TABLE IF EXISTS `system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system` (
  `razon_social` varchar(200) DEFAULT NULL,
  `nombre_comercial` varchar(200) DEFAULT NULL,
  `id_tributario` varchar(30) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `correo_electronico` varchar(35) DEFAULT NULL,
  `periodo_fiscal` char(10) DEFAULT NULL,
  `periodo_actual` char(10) DEFAULT NULL,
  `sucursal_predeterminada` int(11) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system`
--

LOCK TABLES `system` WRITE;
/*!40000 ALTER TABLE `system` DISABLE KEYS */;
INSERT INTO `system` VALUES ('Negocios & Mas, S.A de C.V','Negocios & Mas, S.A de C.V','','Alameda Roosevelt No. 2818 San Salvador','','','','1212','1312',1,13.00,1);
/*!40000 ALTER TABLE `system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `talla_producto`
--

DROP TABLE IF EXISTS `talla_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `talla_producto` (
  `talla_estilo_producto` varchar(20) NOT NULL,
  `color` int(11) NOT NULL,
  `talla` decimal(10,2) NOT NULL,
  PRIMARY KEY (`talla_estilo_producto`,`color`,`talla`),
  KEY `fk_color_prod1_idx` (`color`),
  KEY `fk_estilo_prod2_idx` (`talla_estilo_producto`),
  CONSTRAINT `fk_color_prod1` FOREIGN KEY (`color`) REFERENCES `color_producto` (`color`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_estilo_prod2` FOREIGN KEY (`talla_estilo_producto`) REFERENCES `color_producto` (`color_estilo_producto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `talla_producto`
--

LOCK TABLES `talla_producto` WRITE;
/*!40000 ALTER TABLE `talla_producto` DISABLE KEYS */;
INSERT INTO `talla_producto` VALUES ('101',1,10.00),('101',1,11.00),('101',1,12.00),('102',1,14.00),('101',2,13.00),('101',2,14.00),('102',2,15.00),('102',2,16.00);
/*!40000 ALTER TABLE `talla_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teorico_fisico`
--

DROP TABLE IF EXISTS `teorico_fisico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teorico_fisico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estilo` varchar(20) DEFAULT NULL,
  `linea` int(11) DEFAULT NULL,
  `talla` decimal(10,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `bodega` int(11) DEFAULT NULL,
  `periodo_actual` varchar(10) NOT NULL,
  `mes` int(11) NOT NULL,
  `fisico` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_producto_3_idx` (`estilo`),
  KEY `fk_bodega_destino_idx` (`bodega`),
  CONSTRAINT `fk_bodega_destino_Rd` FOREIGN KEY (`bodega`) REFERENCES `bodega` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_3p_Rd` FOREIGN KEY (`estilo`) REFERENCES `producto` (`estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teorico_fisico`
--

LOCK TABLES `teorico_fisico` WRITE;
/*!40000 ALTER TABLE `teorico_fisico` DISABLE KEYS */;
INSERT INTO `teorico_fisico` VALUES (1,'101',1,12.00,1,9,1,'1312',12,0),(2,'101',1,11.00,1,148,1,'1312',12,0),(3,'101',1,13.00,2,8,1,'1312',12,1),(4,'102',2,16.00,2,17,1,'1312',12,0),(5,'101',1,14.00,2,2,1,'1312',12,3),(6,'101',1,10.00,1,2,1,'1312',12,2),(7,'101',1,11.00,1,17,4,'1312',12,0),(8,'101',1,12.00,1,9,1,'1312',1,0),(9,'101',1,11.00,1,137,3,'1312',1,400),(10,'101',1,13.00,2,8,1,'1312',1,0),(11,'102',2,16.00,2,17,1,'1312',1,0),(12,'101',1,14.00,2,2,1,'1312',1,0),(13,'101',1,10.00,1,20,1,'1312',1,45),(14,'101',1,11.00,1,25,4,'1312',1,400),(15,'101',1,10.00,1,6,4,'1312',1,45);
/*!40000 ALTER TABLE `teorico_fisico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transacciones`
--

DROP TABLE IF EXISTS `transacciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transacciones` (
  `cod` char(2) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transacciones`
--

LOCK TABLES `transacciones` WRITE;
/*!40000 ALTER TABLE `transacciones` DISABLE KEYS */;
INSERT INTO `transacciones` VALUES ('1A','Devoluciones'),('1B','Compras'),('1C','Ingreso por traslado'),('1D','Ingreso por ajuste'),('1E','otros ingresos'),('2B','Salida por traslado'),('2D','Salidas por ajuste'),('2E','otras salidas'),('CA','cambios'),('D','inventario fisico');
/*!40000 ALTER TABLE `transacciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traslado`
--

DROP TABLE IF EXISTS `traslado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traslado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `proveedor_origen` int(11) NOT NULL,
  `proveedor_nacional` int(11) NOT NULL,
  `bodega_origen` int(11) NOT NULL,
  `bodega_destino` int(11) NOT NULL,
  `concepto` varchar(200) DEFAULT NULL,
  `transaccion` char(2) NOT NULL,
  `total_pares` int(11) NOT NULL DEFAULT '0',
  `total_costo` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_costo_p` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_pares_p` int(11) NOT NULL DEFAULT '0',
  `editable` tinyint(1) DEFAULT '1',
  `consigna` tinyint(1) NOT NULL DEFAULT '0',
  `usuario` varchar(20) NOT NULL,
  `concepto_alternativo` varchar(200) DEFAULT NULL,
  `cliente` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `proveedor_origen` (`proveedor_origen`),
  KEY `proveedor_nacional` (`proveedor_nacional`),
  KEY `bodega_origen` (`bodega_origen`),
  KEY `bodega_destino` (`bodega_destino`),
  CONSTRAINT `traslado_ibfk_1` FOREIGN KEY (`proveedor_origen`) REFERENCES `proveedor` (`id`),
  CONSTRAINT `traslado_ibfk_2` FOREIGN KEY (`proveedor_nacional`) REFERENCES `proveedor` (`id`),
  CONSTRAINT `traslado_ibfk_3` FOREIGN KEY (`bodega_origen`) REFERENCES `bodega` (`id`),
  CONSTRAINT `traslado_ibfk_4` FOREIGN KEY (`bodega_destino`) REFERENCES `bodega` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traslado`
--

LOCK TABLES `traslado` WRITE;
/*!40000 ALTER TABLE `traslado` DISABLE KEYS */;
INSERT INTO `traslado` VALUES (16,'2014-01-02',1,1,1,1,'Resurtido','1E',20,340.00,340.00,20,0,0,'admin','',0),(17,'2014-01-02',1,1,1,4,'Consigna Para Cliente William David Parras Mendez Ingreso a la bodega \"Otra\" de la bodega \"bodega Principal\" ','1C',0,0.00,0.00,0,0,1,'admin','Consigna Para Cliente William David Parras Mendez Traslado de la bodega \"bodega Principal\" a la bodega \"Otra\" ',2),(18,'2014-01-02',1,1,1,4,'Consigna Para Cliente William David Parras Mendez Ingreso a la bodega \"Otra\" de la bodega \"bodega Principal\" ','1C',1,5.50,5.50,1,0,1,'admin','Consigna Para Cliente William David Parras Mendez Traslado de la bodega \"bodega Principal\" a la bodega \"Otra\" ',2),(19,'2014-01-06',1,1,1,4,'Consigna Para Cliente William David Parras Mendez Ingreso a la bodega \"Otra\" de la bodega \"bodega Principal\" ','1C',1,7.65,7.65,1,0,1,'admin','Consigna Para Cliente William David Parras Mendez Traslado de la bodega \"bodega Principal\" a la bodega \"Otra\" ',2),(20,'2014-01-06',1,1,1,1,'prueba','1C',2,11.00,5.50,1,1,0,'admin','',0);
/*!40000 ALTER TABLE `traslado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` varchar(40) NOT NULL,
  `clave` varchar(400) NOT NULL,
  `permiso` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('admin','SUE8a7vKyVA7amjZ8RwsnY98d31dfexjpKR8NQ783Mc=',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-06 16:35:24
