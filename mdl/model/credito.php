<?php

class creditoModel extends object {

    public function __construct() {
        
    }

    public function anulacion_recibo($referencia) {

        /* marcar recibo como anulado */
        $oRecibo = $this->get_child('recibo');
        $oRecibo->get($referencia);
        $cliente = $oRecibo->get_attr('cliente');
        $oRecibo->set_attr('anulado', true);
        $oRecibo->save();

        /* revertir saldos de facturas */
        $query = "SELECT * FROM detalle_recibo WHERE id_recibo=$referencia";
        data_model()->executeQuery($query);
        $detalles = array();

        while ($ret = data_model()->getResult()->fetch_assoc()) {
            $detalles[] = $ret;
        }

        $ofactura = $this->get_sibling('factura');
        $ointeres = $this->get_child('interes');
        $abono = 0;

        foreach ($detalles as $detalle) {
            if ($detalle['interes'] == 1) {
                // procesamos interes
                $ointeres->get($detalle['pedido']);
                $ointeres->set_attr('saldo', $ointeres->get_attr('saldo') + $detalle['abono']);
                $ointeres->set_attr('cobro', $ointeres->get_attr('cobro') - $detalle['abono']);
                $ointeres->set_attr('completo_abonado', '0');
                $ointeres->set_attr('credito_pagada', '0');
                $ointeres->save();
            } else {
                // procesamos factura
                $ofactura->get($detalle['pedido']);
                $ofactura->set_attr('saldo', $ofactura->get_attr('saldo') + $detalle['abono']);
                $ofactura->set_attr('cobro', $ofactura->get_attr('cobro') - $detalle['abono']);
                $ofactura->set_attr('completo_abonado', '0');
                $ofactura->set_attr('credito_pagada', '0');
                $ofactura->save();
            }

            $abono += $detalle['abono'];
        }

        /* actualizar cuenta del cliente */
        $oCliente = $this->get_sibling('cliente');
        $oCliente->get($cliente);
        $oCliente->set_attr('credito_usado', $oCliente->get_attr('credito_usado') + $abono);
        $oCliente->save();
    }

    public function anulacion_nota($referencia) {

        /* marcar recibo como anulado */
        $oRecibo = $this->get_child('nota_credito');
        $oRecibo->get($referencia);
        $cliente = $oRecibo->get_attr('cliente');
        $oRecibo->set_attr('anulado', true);
        $oRecibo->save();

        /* revertir saldos de facturas */
        $query = "SELECT * FROM detalle_nota_credito WHERE id_nota_credito=$referencia";
        data_model()->executeQuery($query);
        $detalles = array();

        while ($ret = data_model()->getResult()->fetch_assoc()) {
            $detalles[] = $ret;
        }

        $ofactura = $this->get_sibling('factura');
        $ointeres = $this->get_child('interes');
        $abono = 0;

        foreach ($detalles as $detalle) {
            if ($detalle['interes'] == 1) {
                // procesamos interes
                $ointeres->get($detalle['pedido']);
                $ointeres->set_attr('saldo', $ointeres->get_attr('saldo') + $detalle['abono']);
                $ointeres->set_attr('cobro', $ointeres->get_attr('cobro') - $detalle['abono']);
                $ointeres->set_attr('completo_abonado', '0');
                $ointeres->set_attr('credito_pagada', '0');
                $ointeres->save();
            } else {
                // procesamos factura
                $ofactura->get($detalle['pedido']);
                $ofactura->set_attr('saldo', $ofactura->get_attr('saldo') + $detalle['abono']);
                $ofactura->set_attr('cobro', $ofactura->get_attr('cobro') - $detalle['abono']);
                $ofactura->set_attr('completo_abonado', '0');
                $ofactura->set_attr('credito_pagada', '0');
                $ofactura->save();
            }

            $abono += $detalle['abono'];
        }

        /* actualizar cuenta del cliente */
        $oCliente = $this->get_sibling('cliente');
        $oCliente->get($cliente);
        $oCliente->set_attr('credito_usado', $oCliente->get_attr('credito_usado') + $abono);
        $oCliente->save();
    }

}

?>