<?php

class etlModel extends object {

    public function __construct() {

        # evitar asociacion con base de datos
    }

    public function reset($tblname) {
        $deleteQuery = "TRUNCATE TABLE $tblname";
        data_model()->executeQuery($deleteQuery);
    }

}

?> 