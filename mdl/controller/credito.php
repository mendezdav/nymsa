<?php

import('mdl.view.credito');
import('mdl.model.credito');

class creditoController extends controller {

    private function validar() {
        if (!Session::ValidateSession())
            HttpHandler::redirect(DEFAULT_DIR);
        if (!isset($_SESSION['credito']))
            HttpHandler::redirect('/nymsa/modulo/listar');
    }

    public function test() {
        $this->view->test();
    }

    public function principal() {
        $this->validar();
        $this->view->principal(Session::singleton()->getUser());
    }

    public function mantenimiento() {
        $this->validar();
        $cache = array();
        $cache[0] = $this->model->get_child('tipo_solicitud')->get_list();
        $this->view->mantenimiento(Session::singleton()->getUser(), $cache);
    }

    public function generar_solicitud() {
        $ret = array();
        $ret['message'] = "Solicitud procesada con exito";
        switch ($_POST['tipo_solicitud']) {
            case 1:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $cliente->set_attr('credito', $_POST['monto']);
                    $cliente->set_attr('tcredito', true);
                    $cliente->save();
                    $this->guardar_solicitud($_POST);
                } else {
                    $ret['message'] = "Usuario ya tiene credito";
                }
                break;
            case 2:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $ret["message"] = "cliente no tiene credito";
                } else {
                    if ($cliente->get_attr('extra_credito') != 1) {
                        $cliente->set_attr('extra_credito', true);
                        $cliente->set_attr('monto_extra', $_POST['monto']);
                        $cliente->save();
                        $this->guardar_solicitud($_POST);
                    } else {
                        $ret["message"] = "Ya tiene financiamiento";
                    }
                }
                break;
            case 3:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $ret['message'] = "No tiene credito";
                } else {
                    $monto_actual = $cliente->get_attr('credito');
                    $cliente->set_attr('credito', $monto_actual + $_POST['monto']);
                    $cliente->save();
                    $this->guardar_solicitud($_POST);
                }
                break;
            case 4:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $ret['message'] = "No tiene credito";
                } else {
                    $monto_actual = $cliente->get_attr('credito');
                    if ($monto_actual > $_POST['monto']) {
                        $cliente->set_attr('credito', $monto_actual - $_POST['monto']);
                        $cliente->save();
                        $this->guardar_solicitud($_POST);
                    } else {
                        $ret['message'] = "No se puede completar la transaccion";
                    }
                }
                break;
            case 5:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $ret['message'] = "No tiene credito";
                } else {
                    $cliente->set_attr('credito', 0);
                    $cliente->set_attr('monto_extra', 0);
                    $cliente->set_attr('tcredito', 0);
                    $cliente->set_attr('extra_credito', 0);
                    $cliente->save();
                    $this->guardar_solicitud($_POST);
                }
                break;
            case 6:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $cliente->set_attr('tcredito', true);
                    $cliente->set_attr('extra_credito', true);
                    $cliente->save();
                    $this->guardar_solicitud($_POST);
                } else {
                    $ret['message'] = "Credito esta activado";
                }
                break;
            case 7:
                $cliente = $this->model->get_sibling('cliente');
                $cliente->get($_POST['cliente']);
                if ($cliente->get_attr('tcredito') != 1) {
                    $ret['message'] = "Credito esta desactivado";
                } else {
                    $cliente->set_attr('tcredito', 0);
                    $cliente->set_attr('extra_credito', 0);
                    $cliente->save();
                    $this->guardar_solicitud($_POST);
                }
                break;
        }

        echo json_encode($ret);
    }

    public function cobros() {
        $this->validar();
        $tieneCaja = false;
        list($tieneCaja, $data) = $this->model->get_sibling('factura')->tieneCaja(Session::singleton()->getUser());
        if (!$tieneCaja)
            HttpHandler::redirect('/nymsa/credito/principal?error=900');
        $cache = array();
		$serie = $this->model->get_child('serie');
		if($serie->exists($data['serie_recibo'])){
			$serie->get($data['serie_recibo']);
			$ultimo_recibo = $serie->get_attr('ultimo_utilizado');
			$this->view->cobros(Session::singleton()->getUser(), $data, $ultimo_recibo);
		}else{
			HttpHandler::redirect('/nymsa/credito/principal?error=900');
		}
	}

    public function notas_de_credito() {
        $this->validar();
        $tieneCaja = false;
        list($tieneCaja, $data) = $this->model->get_sibling('factura')->tieneCaja(Session::singleton()->getUser());
        if (!$tieneCaja)
            HttpHandler::redirect('/nymsa/credito/principal?error=900');
        $cache = array();
        $this->view->notas_de_credito(Session::singleton()->getUser(), $data);
    }

    function existe_recibo($recibo, $caja) {
        $query = "SELECT referencia FROM caja_recibo_referencia WHERE pedido=$recibo AND caja=$caja";
        data_model()->executeQuery($query);
        if (data_model()->getNumRows() > 0) {
            $data = data_model()->getResult()->fetch_assoc();
            return array(true, $data['referencia']);
        } else {
            return array(false, 0);
        }
    }

    function existe_nota_credito($nota_credito, $caja) {
        $query = "SELECT referencia FROM caja_nota_credito_referencia WHERE pedido=$nota_credito AND caja=$caja";
        data_model()->executeQuery($query);
        if (data_model()->getNumRows() > 0) {
            $data = data_model()->getResult()->fetch_assoc();
            return array(true, $data['referencia']);
        } else {
            return array(false, 0);
        }
    }

    public function inicializar_recibo() {
        $data = $_POST;

        /* obtiene el cliente que efectua el abono */
        $oCliente = $this->model->get_sibling('cliente');
        $oCliente->get($data['cliente']);
        $cliente  = $data['cliente'];

        /* verifica si existe el recibo */
        list($existe, $referencia) = $this->existe_recibo($data['id'], $data['caja']);

        /* obtiene la caja de donde se realiza el abono */
        $oCaja = $this->model->get_child('caja');
        $oCaja->get($data['caja']);

        /* si no existe el pedido */
        if (!$existe) {
            // aumenta el contador del último cobro realizado
            $oCaja->set_attr('ultimo_cobro', $oCaja->get_attr('ultimo_cobro') + 1);
            $oCaja->save();
        }

        $data['nombre_cliente'] = $oCliente->get_attr('primer_nombre') . ' ' . $oCliente->get_attr('primer_apellido');
        
        /* obtiene una referencia al recivo */
        $oRecibo = $this->model->get_child('recibo');
        $oRecibo->get($referencia); // referencia es cero si no existe ningún recibo

        if ($data['id'] == 0) {
            $query = "SELECT saldo,cobro FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
					  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND completo_abonado=0 AND estado='FACTURADO'";
            data_model()->executeQuery($query);

            $total_pendiente = 0;

            while ($da = data_model()->getResult()->fetch_assoc()) {
                $total_pendiente += $da['saldo'];
            }

            $data['saldo_anterior'] = $total_pendiente;
            $data['nuevo_saldo']    = $total_pendiente;
            $oRecibo->change_status($data);
            $oRecibo->save();
        }

        $response = array();
        $fields = $oRecibo->get_fields();
        foreach ($fields as $field) {
            $response[$field] = $oRecibo->get_attr($field);
        }

        if ($data['id'] == 0) {
            $response['id'] = $oRecibo->last_insert_id();
        }

        if (!$existe) {
            $p['caja'] = $data['caja'];
            $p['pedido'] = $oCaja->get_attr('ultimo_cobro');
            $p['referencia'] = $response['id'];
            $oj = $this->model->get_child('caja_recibo_referencia');
            $oj->get(0);
            $oj->change_status($p);
            $oj->save();
        }

        $response['muestra'] = $oCaja->get_attr('ultimo_cobro');

        echo json_encode($response);
    }

    public function inicializar_nota_credito() {
        $data = $_POST;
        $data['id'] = (empty($data['id'])) ? 0 : $data['id'];

        $oCliente = $this->model->get_sibling('cliente');
        $oCliente->get($data['cliente']);
        $cliente = $data['cliente'];

        list($existe, $referencia) = $this->existe_nota_credito($data['id'], $data['caja']);

        $oCaja = $this->model->get_child('caja');
        $oCaja->get($data['caja']);

        if (!$existe) {
            $oCaja->set_attr('ultima_nota_credito', $oCaja->get_attr('ultima_nota_credito') + 1);
            $oCaja->save();
        }
        $data['nombre_cliente'] = $oCliente->get_attr('primer_nombre') . ' ' . $oCliente->get_attr('primer_apellido');
        $oNota = $this->model->get_child('nota_credito');
        $oNota->get($referencia);

        if ($data['id'] == 0) {
            $query = "SELECT saldo,cobro FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
					  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND completo_abonado=0 AND estado='FACTURADO'";
            data_model()->executeQuery($query);
            $total_pendiente = 0;
            while ($da = data_model()->getResult()->fetch_assoc()) {
                $total_pendiente += $da['saldo'];
            }
            $data['saldo_anterior'] = $total_pendiente;
            $data['nuevo_saldo'] = $total_pendiente;
            $oNota->change_status($data);
            $oNota->save();
        }
        $response = array();
        $fields = $oNota->get_fields();
        foreach ($fields as $field) {
            $response[$field] = $oNota->get_attr($field);
        }

        if ($data['id'] == 0) {
            $response['id'] = $oNota->last_insert_id();
        }

        if (!$existe) {
            $p['caja'] = $data['caja'];
            $p['pedido'] = $oCaja->get_attr('ultima_nota_credito');
            $p['referencia'] = $response['id'];
            $oj = $this->model->get_child('caja_nota_credito_referencia');
            $oj->get(0);
            $oj->change_status($p);
            $oj->save();
        }

        $response['muestra'] = $oCaja->get_attr('ultima_nota_credito');

        echo json_encode($response);
    }

    public function cliente_recibo() {
        if ($_POST['id'] != 0 && !empty($_POST['id'])) {
            list($existe, $referencia) = $this->existe_recibo($_POST['id'], $_POST['caja']);
            $oRecibo = $this->model->get_child('recibo');
            $oRecibo->get($referencia);
            $response = array();
            $response['cliente'] = $oRecibo->get_attr('cliente');
            $response['referencia'] = $referencia;
            echo json_encode($response);
        }
    }

    public function cliente_nota_credito() {
        if ($_POST['id'] != 0 && !empty($_POST['id'])) {
            list($existe, $referencia) = $this->existe_nota_credito($_POST['id'], $_POST['caja']);
            $oNota = $this->model->get_child('nota_credito');
            $oNota->get($referencia);
            $response = array();
            $response['cliente'] = $oNota->get_attr('cliente');
            $response['referencia'] = $referencia;
            echo json_encode($response);
        }
    }

    public function cargar_saldo() {
        $oRecibo = $this->model->get_child('recibo');
        $oRecibo->get($_POST['id']);
        $cliente = $oRecibo->get_attr('cliente');
        $query = "SELECT saldo,total FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
					  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND estado='FACTURADO'";
        data_model()->executeQuery($query);
        $total_pendiente = 0;
        $total_monto = 0;
        while ($da = data_model()->getResult()->fetch_assoc()) {
            $total_pendiente += $da['saldo'];
            $total_monto += $da['total'];
        }

        $query = "SELECT saldo,total FROM interes
					  WHERE id_cliente=$cliente AND credito_pagada=0 ";
        data_model()->executeQuery($query);
        while ($da = data_model()->getResult()->fetch_assoc()) {
            $total_pendiente += $da['saldo'];
            $total_monto += $da['total'];
        }

        $response = array();
        $response['total'] = $total_monto;
        $response['saldo'] = $total_pendiente;

        echo json_encode($response);
    }

    public function cargar_saldo_nota() {
        $oNota = $this->model->get_child('nota_credito');
        $oNota->get($_POST['id']);
        $cliente = $oNota->get_attr('cliente');
        $query = "SELECT saldo,total FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
					  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND estado='FACTURADO'";
        data_model()->executeQuery($query);
        $total_pendiente = 0;
        $total_monto = 0;
        while ($da = data_model()->getResult()->fetch_assoc()) {
            $total_pendiente += $da['saldo'];
            $total_monto += $da['total'];
        }

        $query = "SELECT saldo,total FROM interes
					  WHERE id_cliente=$cliente AND credito_pagada=0 ";
        data_model()->executeQuery($query);
        while ($da = data_model()->getResult()->fetch_assoc()) {
            $total_pendiente += $da['saldo'];
            $total_monto += $da['total'];
        }

        $response = array();
        $response['total'] = $total_monto;
        $response['saldo'] = $total_pendiente;

        echo json_encode($response);
    }

    public function abonar() {

        $id_recibo   = 1; // referencia al recibo
        $oRecibo     = $this->model->get_child('recibo'); // modelo
        list($existe, $referencia) = $this->existe_recibo($id_recibo, 2); // obtiene la referencia
        $oRecibo->get($referencia); // inicializa el modelo 

        $cliente     = $oRecibo->get_attr('cliente');
        $pendientes  = array();
        $ofactura    = $this->model->get_sibling('factura');
        $ointeres    = $this->model->get_child('interes');
        $n_pendiente = 0;
        $interes     = false;
        $pagos       = null;
        $abono       = 40.00;


        $ccdiahmdl    = $this->model->get_child('ccdiah');

        $pagos = $ccdiahmdl->pagosPendientes($cliente);

        if($pagos!==null){
            $total = 0;
            foreach ($pagos as $pago) {
                while($abono > 0 && $pago['saldo'] > 0){
                    if($abono > $pago['saldo']){
                        $total = $pago['saldo'];
                    }else{
                        $total = $abono;
                    }
                    
                    $pago['saldo']-= $total;
                    $abono = $abono - $total;
                    $ccdiahmdl->get($pago['id']);
                    $ccdiahmdl->saldo = $pago['saldo'];
                    $ccdiahmdl->abono = $total;
                }
            }
        }

        /*if (!$interes) {
            $query = "SELECT 'FACTURA' as TP, id,saldo,id_factura,cobro,total FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
						  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND completo_abonado=0 AND estado='FACTURADO'";

            data_model()->executeQuery($query);
            $n_pendiente = data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }

            $query = "SELECT 'INTERES' as TP,id_factura,saldo,cobro,total  FROM interes WHERE id_cliente=$cliente AND credito_pagada=0 AND completo_abonado = 0 ";

            data_model()->executeQuery($query);
            $n_pendiente += data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }
        } else {

            $query = "SELECT 'INTERES' as TP,id_factura,saldo,cobro,total  FROM interes WHERE id_cliente=$cliente AND credito_pagada=0 AND completo_abonado = 0 ";

            data_model()->executeQuery($query);

            $n_pendiente = data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }

            $query = "SELECT 'FACTURA' as TP, id,saldo,id_factura,cobro,total FROM factura INNER JOIN id_facturas ON id_factura=id_pedido WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND completo_abonado=0 AND estado='FACTURADO'";

            data_model()->executeQuery($query);

            $n_pendiente += data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }
        }*/

        /*$cnt = 0;
        $total_debe = 0;

        foreach ($pendientes as $pendiente) {
            $total_debe += $pendiente['saldo'];
        }

        $nuevo_saldo = $total_debe - 10;
        $abono = 10;
        $oRecibo->set_attr('abono', $oRecibo->get_attr('abono') + $abono);
        
        while ($abono > 0 && $n_pendiente > 0) {
            $cobro = array();
            $cobro['concepto'] = $_POST['concepto'];
            if ($pendientes[$cnt]['TP'] == 'FACTURA') {
                $cobro['descripcion'] = "Abono a Factura No." . $pendientes[$cnt]['id'];
                //$cobro['interes'] = 0;
            } else {
                $cobro['descripcion'] = "Pago de interes por mora";
                $cobro['interes'] = true;
            }
            $cobro['id_recibo'] = $id_recibo;
            $cobro['pedido'] = $pendientes[$cnt]['id_factura'];

            if ($abono >= $pendientes[$cnt]['saldo']) {
                if ($pendientes[$cnt]['TP'] == 'FACTURA') {
                    $ofactura->get($pendientes[$cnt]['id_factura']);
                    $abono -= $ofactura->get_attr('saldo');
                    $ofactura->set_attr('saldo', 0.0);
                    $ofactura->set_attr('cobro', $ofactura->get_attr('total'));
                    $cobro['abono'] = $pendientes[$cnt]['saldo'];
                    $ofactura->set_attr('completo_abonado', true);
                    $ofactura->save();
                    $n_pendiente--;
                } else {
                    $ointeres->get($pendientes[$cnt]['id_factura']);
                    $abono -= $ointeres->get_attr('saldo');
                    $ointeres->set_attr('saldo', 0.0);
                    $ointeres->set_attr('cobro', $ointeres->get_attr('total'));
                    $cobro['abono'] = $pendientes[$cnt]['saldo'];
                    $ointeres->set_attr('completo_abonado', true);
                    $ointeres->save();
                    $n_pendiente--;
                }
            } else {
                $cobro['abono'] = $abono;
                if ($pendientes[$cnt]['TP'] == 'FACTURA') {
                    $ofactura->get($pendientes[$cnt]['id_factura']);
                    $ofactura->set_attr('saldo', $ofactura->get_attr('saldo') - $abono);
                    $ofactura->set_attr('cobro', $ofactura->get_attr('cobro') + $abono);
                    $abono = 0;
                    $ofactura->save();
                } else {
                    $ointeres->get($pendientes[$cnt]['id_factura']);
                    $ointeres->set_attr('saldo', $ointeres->get_attr('saldo') - $abono);
                    $ointeres->set_attr('cobro', $ointeres->get_attr('cobro') + $abono);
                    $abono = 0;
                    $ointeres->save();
                }
            }

            $oRecibo->set_attr('abono', $oRecibo->get_attr('abono') - $abono);
            $detalle = $this->model->get_child('detalle_recibo');
            $detalle->get(0);
            $detalle->change_status($cobro);
            $detalle->save();
            $oRecibo->set_attr('saldo_anterior', $total_debe);
            $oRecibo->set_attr('nuevo_saldo', $nuevo_saldo);
            $oRecibo->set_attr('cobros_pendientes', $n_pendiente);
            $oRecibo->save();
            $cnt++;
        }*/
    }

    public function abonar_con_nota() {

        $id_nota_credito = $_POST['id_nota_credito'];
        $oNota = $this->model->get_child('nota_credito');
        $oNota->get($id_nota_credito);
        $cliente = $oNota->get_attr('cliente');
        $pendientes = array();
        $ofactura = $this->model->get_sibling('factura');
        $ointeres = $this->model->get_child('interes');
        $n_pendiente = 0;
        $interes = $_POST['interes'];

        if (!$interes) {
            $query = "SELECT 'FACTURA' as TP, id,saldo,id_factura,cobro,total FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
						  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND completo_abonado=0 AND estado='FACTURADO'";

            data_model()->executeQuery($query);
            $n_pendiente = data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }

            $query = "SELECT 'INTERES' as TP,id_factura,saldo,cobro,total  FROM interes WHERE id_cliente=$cliente AND credito_pagada=0 AND completo_abonado = 0 ";

            data_model()->executeQuery($query);
            $n_pendiente += data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }
        } else {

            $query = "SELECT 'INTERES' as TP,id_factura,saldo,cobro,total  FROM interes WHERE id_cliente=$cliente AND credito_pagada=0 AND completo_abonado = 0 ";

            data_model()->executeQuery($query);
            $n_pendiente = data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }

            $query = "SELECT 'FACTURA' as TP, id,saldo,id_factura,cobro,total FROM factura INNER JOIN id_facturas ON id_factura=id_pedido 
						  WHERE id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND completo_abonado=0 AND estado='FACTURADO'";

            data_model()->executeQuery($query);
            $n_pendiente += data_model()->getNumRows();

            while ($data = data_model()->getResult()->fetch_assoc()) {
                $pendientes[] = $data;
            }
        }

        $cnt = 0;
        $total_debe = 0;

        foreach ($pendientes as $pendiente) {
            $total_debe += $pendiente['saldo'];
        }

        $nuevo_saldo = $total_debe - $_POST['abono'];
        $abono = $_POST['abono'];
        $oNota->set_attr('abono', $oNota->get_attr('abono') + $abono);

        while ($abono > 0 && $n_pendiente > 0) {
            $cobro = array();
            $cobro['concepto'] = $_POST['concepto'];
            if ($pendientes[$cnt]['TP'] == 'FACTURA') {
                $cobro['descripcion'] = "Abono a Factura No." . $pendientes[$cnt]['id'];
                //$cobro['interes'] = 0;
            } else {
                $cobro['descripcion'] = "Pago de interes por mora";
                $cobro['interes'] = true;
            }
            $cobro['id_nota_credito'] = $id_nota_credito;
            $cobro['pedido'] = $pendientes[$cnt]['id_factura'];

            if ($abono >= $pendientes[$cnt]['saldo']) {
                if ($pendientes[$cnt]['TP'] == 'FACTURA') {
                    $ofactura->get($pendientes[$cnt]['id_factura']);
                    $abono -= $ofactura->get_attr('saldo');
                    $ofactura->set_attr('saldo', 0.0);
                    $ofactura->set_attr('cobro', $ofactura->get_attr('total'));
                    $cobro['abono'] = $pendientes[$cnt]['saldo'];
                    $ofactura->set_attr('completo_abonado', true);
                    $ofactura->save();
                    $n_pendiente--;
                } else {
                    $ointeres->get($pendientes[$cnt]['id_factura']);
                    $abono -= $ointeres->get_attr('saldo');
                    $ointeres->set_attr('saldo', 0.0);
                    $ointeres->set_attr('cobro', $ointeres->get_attr('total'));
                    $cobro['abono'] = $pendientes[$cnt]['saldo'];
                    $ointeres->set_attr('completo_abonado', true);
                    $ointeres->save();
                    $n_pendiente--;
                }
            } else {
                $cobro['abono'] = $abono;
                if ($pendientes[$cnt]['TP'] == 'FACTURA') {
                    $ofactura->get($pendientes[$cnt]['id_factura']);
                    $ofactura->set_attr('saldo', $ofactura->get_attr('saldo') - $abono);
                    $ofactura->set_attr('cobro', $ofactura->get_attr('cobro') + $abono);
                    $abono = 0;
                    $ofactura->save();
                } else {
                    $ointeres->get($pendientes[$cnt]['id_factura']);
                    $ointeres->set_attr('saldo', $ointeres->get_attr('saldo') - $abono);
                    $ointeres->set_attr('cobro', $ointeres->get_attr('cobro') + $abono);
                    $abono = 0;
                    $ointeres->save();
                }
            }

            $oNota->set_attr('abono', $oNota->get_attr('abono') - $abono);
            $detalle = $this->model->get_child('detalle_nota_credito');
            $detalle->get(0);
            $detalle->change_status($cobro);
            $detalle->save();
            $oNota->set_attr('saldo_anterior', $total_debe);
            $oNota->set_attr('nuevo_saldo', $nuevo_saldo);
            $oNota->set_attr('cobros_pendientes', $n_pendiente);
            $oNota->save();
            $cnt++;
        }
    }

    public function guardar_recibo($id) {
        $oRecibo = $this->model->get_child('recibo');
        $oRecibo->get($id);
        $oRecibo->set_attr('aplicado', 1);
        $oRecibo->save();
        $oRecibo->actualizar_saldos($id, $oRecibo->get_attr('cliente'));
        $serie = $oRecibo->get_attr('serie_recibo');
        $query = "UPDATE serie SET ultimo_utilizado = (ultimo_utilizado + 1) WHERE id = $serie ";
        data_model()->executeQuery($query);
        $query = "SELECT ultimo_utilizado FROM serie WHERE id = $serie";
        data_model()->executeQuery($query);
        $ret = data_model()->getResult()->fetch_assoc();
        $ultimo = $ret['ultimo_utilizado'];
        $query = "INSERT INTO id_recibos VALUES( $ultimo,$serie,$id )";
        data_model()->executeQuery($query);
        HttpHandler::redirect('/nymsa/credito/cobros');
    }

    public function guardar_nota($id) {
        $oNota = $this->model->get_child('nota_credito');
        $oNota->get($id);
        $oNota->set_attr('aplicado', 1);
        $oNota->save();
        $oNota->actualizar_saldos($id, $oNota->get_attr('cliente'));
        $serie = $oNota->get_attr('serie_nota_credito');
        $query = "UPDATE serie SET ultimo_utilizado = (ultimo_utilizado + 1) WHERE id = $serie ";
        data_model()->executeQuery($query);
        $query = "SELECT ultimo_utilizado FROM serie WHERE id = $serie";
        data_model()->executeQuery($query);
        $ret = data_model()->getResult()->fetch_assoc();
        $ultimo = $ret['ultimo_utilizado'];
        $query = "INSERT INTO id_nota_creditos VALUES( $ultimo,$serie,$id )";
        data_model()->executeQuery($query);
        HttpHandler::redirect('/nymsa/credito/notas_de_credito');
    }

    private function guardar_solicitud($array) {
        $data = $array;
        $solicitud = $this->model->get_child('solicitud');
        $data['operado_por'] = Session::singleton()->getUser();
        $solicitud->get($data['id'] * 1);
        $solicitud->change_status($data);
        $solicitud->save();
    }

    public function anular_recibos() {
        $this->validar();
        $this->view->anular_recibos(Session::singleton()->getUser());
    }

    public function anular_nota_credito() {
        $this->validar();
        $this->view->anular_nota_credito(Session::singleton()->getUser());
    }

    public function prueba() {
        $cliente = $this->model->get_sibling('cliente');
        $cliente->get(3);
        echo var_dump($cliente->tiene_credito($json_response = true));
        $cliente->anular_credito($json_response = true);
        $cliente->desactivar_credito($json_response = true);
    }

    public function recibo_valido() {
        $caja = $_POST['caja'];
        $serie = $_POST['serie'];
        $numero = $_POST['numero'];
        $response = array();
        $response['existe'] = false;
        $query = "SELECT id_recibos.id  as id, codigo_recibo as serie, caja, nombre_cliente as nombre_cliente,cliente, abono, fecha,recibo.id as referencia, anulado  from recibo join id_recibos on recibo.id = id_recibos.id_recibo WHERE id_recibos.id=$numero AND caja=$caja AND codigo_recibo='{$serie}'";
        data_model()->executeQuery($query);
        if (data_model()->getNumRows() > 0) {
            $response['existe'] = true;
        }

        $ret = data_model()->getResult()->fetch_assoc();

        $response['fecha'] = $ret['fecha'];
        $response['cliente'] = $ret['cliente'];
        $response['abono'] = $ret['abono'];
        $response['referencia'] = $ret['referencia'];
        $response['anulado'] = ( $ret['anulado'] == 1 ) ? true : false;

        echo json_encode($response);
    }

    public function nota_valido() {
        $caja = $_POST['caja'];
        $serie = $_POST['serie'];
        $numero = $_POST['numero'];
        $response = array();
        $response['existe'] = false;
        $query = "SELECT id_nota_creditos.id  as id, codigo_nota_credito as serie, caja, nombre_cliente as nombre_cliente,cliente, abono, fecha,nota_credito.id as referencia, anulado  from nota_credito join id_nota_creditos on nota_credito.id = id_nota_creditos.id_nota_credito WHERE id_nota_creditos.id=$numero AND caja=$caja AND codigo_nota_credito='{$serie}'";
        data_model()->executeQuery($query);
        if (data_model()->getNumRows() > 0) {
            $response['existe'] = true;
        }

        $ret = data_model()->getResult()->fetch_assoc();

        $response['fecha'] = $ret['fecha'];
        $response['cliente'] = $ret['cliente'];
        $response['abono'] = $ret['abono'];
        $response['referencia'] = $ret['referencia'];
        $response['anulado'] = ( $ret['anulado'] == 1 ) ? true : false;

        echo json_encode($response);
    }

    public function anulacion_recibo($referencia) {
        $this->model->anulacion_recibo($referencia);
        HttpHandler::redirect('/nymsa/credito/anular_recibos');
    }

    public function anulacion_nota($referencia) {
        $this->model->anulacion_nota($referencia);
        HttpHandler::redirect('/nymsa/credito/anular_nota_credito');
    }

    public function cargar_recibos() {
        header('Content-type:text/javascript;charset=UTF-8');
        $json = json_decode(stripslashes($_POST["_gt_json"]));
        $pageNo = $json->{'pageInfo'}->{'pageNum'};
        $pageSize = 10; //10 rows per page
        //to get how many records totally.
        $sql = "select count(*) as cnt from recibo join id_recibos on recibo.id = id_recibos.id_recibo";
        $handle = mysql_query($sql);
        $row = mysql_fetch_object($handle);
        $totalRec = $row->cnt;

        //make sure pageNo is inbound
        if ($pageNo < 1 || $pageNo > ceil(($totalRec / $pageSize))):
            $pageNo = 1;
        endif;

        if ($json->{'action'} == 'load'):
            $sql = "select id_recibos.id  as id, codigo_recibo as serie, caja, nombre_cliente as nombre_cliente,cliente, abono, fecha,recibo.id as referencia  from recibo join id_recibos on recibo.id = id_recibos.id_recibo limit  " . ($pageNo - 1) * $pageSize . ", " . $pageSize;
            $handle = mysql_query($sql);
            $retArray = array();
            while ($row = mysql_fetch_object($handle)):
                $retArray[] = $row;
            endwhile;
            $data = json_encode($retArray);
            $ret = "{data:" . $data . ",\n";
            $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
            $ret .= "recordType : 'object'}";
            echo $ret;
        endif;
    }

    public function cargar_notas() {
        header('Content-type:text/javascript;charset=UTF-8');
        $json = json_decode(stripslashes($_POST["_gt_json"]));
        $pageNo = $json->{'pageInfo'}->{'pageNum'};
        $pageSize = 10; //10 rows per page
        //to get how many records totally.
        $sql = "select count(*) as cnt from nota_credito join id_nota_creditos on nota_credito.id = id_nota_creditos.id_nota_credito";
        $handle = mysql_query($sql);
        $row = mysql_fetch_object($handle);
        $totalRec = $row->cnt;

        //make sure pageNo is inbound
        if ($pageNo < 1 || $pageNo > ceil(($totalRec / $pageSize))):
            $pageNo = 1;
        endif;

        if ($json->{'action'} == 'load'):
            $sql = "select id_nota_creditos.id  as id, codigo_nota_credito as serie, caja, nombre_cliente as nombre_cliente,cliente, abono, fecha,nota_credito.id as referencia  from nota_credito join id_nota_creditos on nota_credito.id = id_nota_creditos.id_nota_credito limit  " . ($pageNo - 1) * $pageSize . ", " . $pageSize;
            $handle = mysql_query($sql);
            $retArray = array();
            while ($row = mysql_fetch_object($handle)):
                $retArray[] = $row;
            endwhile;
            $data = json_encode($retArray);
            $ret = "{data:" . $data . ",\n";
            $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
            $ret .= "recordType : 'object'}";
            echo $ret;
        endif;
    }

    public function cargar_cobros($cliente) {
        header('Content-type:text/javascript;charset=UTF-8');
        $json = json_decode(stripslashes($_POST["_gt_json"]));
        $pageNo = $json->{'pageInfo'}->{'pageNum'};
        $pageSize = 10; //10 rows per page
        //to get how many records totally.
        $sql = "select count(*) as cnt from factura join id_facturas on id_factura=id_pedido where id_cliente=$cliente AND tipo = 'CREDITO' AND credito_pagada=0 AND estado='FACTURADO'";
        $handle = mysql_query($sql);
        $row = mysql_fetch_object($handle);
        $totalRec = $row->cnt;

        //make sure pageNo is inbound
        if ($pageNo < 1 || $pageNo > ceil(($totalRec / $pageSize))):
            $pageNo = 1;
        endif;

        if ($json->{'action'} == 'load'):
            $sql = "select * FROM ccdiah WHERE codcli=$cliente AND saldo > 0  limit  " . ($pageNo - 1) * $pageSize . ", " . $pageSize;
            $handle = mysql_query($sql);
            $retArray = array();
            while ($row = mysql_fetch_object($handle)):
                $retArray[] = $row;
            endwhile;
            $data = json_encode($retArray);
            $ret = "{data:" . $data . ",\n";
            $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
            $ret .= "recordType : 'object'}";
            echo $ret;
        endif;
    }

    public function cargar_cobros_hechos($cliente) {
        header('Content-type:text/javascript;charset=UTF-8');
        $json = json_decode(stripslashes($_POST["_gt_json"]));
        $pageNo = $json->{'pageInfo'}->{'pageNum'};
        $pageSize = 10; //10 rows per page
        //to get how many records totally.
        $sql = "select count(*) as cnt from factura join id_facturas on id_factura=id_pedido where id_cliente=$cliente AND tipo = 'CREDITO' AND estado='FACTURADO'";
        $handle = mysql_query($sql);
        $row = mysql_fetch_object($handle);
        $totalRec = $row->cnt;

        //make sure pageNo is inbound
        if ($pageNo < 1 || $pageNo > ceil(($totalRec / $pageSize))):
            $pageNo = 1;
        endif;

        if ($json->{'action'} == 'load'):
            $sql = "select 'FACTURA' as tipo,id,caja,serie,fecha,fecha_vence,total,saldo,cobro,total from factura join id_facturas on id_factura=id_pedido where id_cliente=$cliente AND tipo = 'CREDITO' AND estado='FACTURADO' UNION select 'INTERES' as tipo,id_factura,caja,serie,fecha,fecha_vence,total,saldo,cobro,total from interes WHERE id_cliente=$cliente AND credito_pagada=1 UNION select 'CF' as tipo,id,caja,serie,fecha,fecha_vence,total,saldo,cobro,total from factura join id_creditos_fiscales on id_factura=id_pedido where id_cliente=$cliente AND tipo = 'CF_CREDITO' AND credito_pagada=0 AND estado='FACTURADO' limit  " . ($pageNo - 1) * $pageSize . ", " . $pageSize;
            $handle = mysql_query($sql);
            $retArray = array();
            while ($row = mysql_fetch_object($handle)):
                $retArray[] = $row;
            endwhile;
            $data = json_encode($retArray);
            $ret = "{data:" . $data . ",\n";
            $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
            $ret .= "recordType : 'object'}";
            echo $ret;
        endif;
    }

    /* ETL's */

    public function etl_cabecera_factura() {
        set_time_limit(99999999999999);
        data_model()->newConnection(HOST, USER, PASSWORD, 'dbo');
    }

    public function etl_precio_costo_producto() {
        set_time_limit(99999999999999);

        $query = "SELECT cestilo as estilo, producto_data.linea as linea, ccolor as color, producto_data.talla, precio, costo FROM producto_data INNER JOIN talla_producto on cestilo = talla_estilo_producto AND talla_producto.linea=producto_data.linea AND talla_producto.talla=producto_data.talla AND ccolor=color";
        data_model()->executeQuery($query);

        $colores1 = array();
        $colores2 = array();
        $colores3 = array();
        $colores4 = array();
        $colores5 = array();
        $colores6 = array();
        $colores7 = array();
        $colores8 = array();
        $colores9 = array();
        $colores10 = array();
        $colores11 = array();
        $colores12 = array();
        $colores13 = array();
        $colores14 = array();
        $colores15 = array();
        $colores16 = array();
        $colores17 = array();
        $colores18 = array();
        $colores19 = array();
        $colores20 = array();
        $colores21 = array();
        $colores22 = array();
        $colores23 = array();
        $colores24 = array();
        $colores25 = array();

        $ct = 0;
        $n = 1;

        while ($ret = data_model()->getResult()->fetch_assoc()) {

            $mdl = $ct % 1000;
            if ($mdl == 0) {
                $ct = 0;
                $n++;
            }
            $name = 'colores' . ($n - 1);
            ${$name}[] = $ret;
            $ct++;
        }



        for ($i = 1; $i <= 25; $i++) {
            $name = 'colores' . $i;
            foreach (${$name} as $color) {
                $precio = $color['precio'];
                $costo = $color['costo'];
                $estilo = $color['estilo'];
                $linea = $color['linea'];
                $talla = $color['talla'];
                $color = $color['color'];
                $query = "INSERT INTO control_precio values(null,$estilo,$linea,$talla,$color,$precio,$costo)";
                data_model()->executeQuery($query);
            }
        }
    }

    public function etl_stock() {
        set_time_limit(99999999999999);
        $archivo = file('/media/Datos/Nymsa/stock.csv');
        $c = 0;
        foreach ($archivo as $linea) {
            if($c!=0){
                $campos = explode(";", $linea);
                for($i = 0; $i < count($campos); $i++){
                    if($i!=3){
                        $tmp = explode(' ', $campos[$i]);
                        $campos[$i] = $tmp[0] * 1;
                    }
                }

                $bodega = $campos[0];
                $linea  = $campos[1];
                $estilo = $campos[3];
                $color  = $campos[5];
                $talla  = $campos[6];
                $stock  = $campos[7];

                $query = "INSERT INTO estado_bodega VALUES(null, '{$estilo}', $linea, $talla, $color, $stock, $bodega)";
                data_model()->executeQuery($query);
            }else{
                $c++;
            }
        }
    }

    public function etl_costos_precios() {
        set_time_limit(99999999999999);
        $archivo = file('/media/Datos/Nymsa/stock.csv');
        $c = 0;
        foreach ($archivo as $linea) {
            if($c!=0){
                $campos = explode(";", $linea);
                for($i = 0; $i < count($campos); $i++){
                    if($i!=3){
                        $tmp = explode(' ', $campos[$i]);
                        $campos[$i] = $tmp[0] * 1;
                    }
                }

                $bodega = $campos[0];
                $linea  = $campos[1];
                $estilo = $campos[3];
                $color  = $campos[5];
                $talla  = $campos[6];
                
                $precio  = $campos[11];
                $costo   = $campos[12];

                $query = "INSERT INTO control_precio VALUES(null, '{$estilo}', $linea, $talla, $color, $precio, $costo)";
                data_model()->executeQuery($query);
            }else{
                $c++;
            }
        }
    }

    public function etl_clientes() {
        set_time_limit(99999999999999);
        $oData = $this->model->get_child('cliente_data');
        $oFinal = $this->model->get_child('cliente');
        $clientes = $oData->get_list_array();
        foreach ($clientes as $cliente) {
            while (($cliente['codcli'] - 1) > $oFinal->last_insert_id()) {
                $data = array();
                $campos = $oData->get_fields();
                foreach ($campos as $campo) {
                    if ($campo != 'codcli' && $campo != 'nombre_completo') {
                        $data[$campo] = NULL;
                    }
                }
                $oFinal->get(0);
                $oFinal->change_status($data);
                $oFinal->save();
            }

            unset($cliente['codcli']);
            unset($cliente['nombre_completo']);
            $oFinal->get(0);
            $oFinal->change_status($cliente);
            $oFinal->save();
        }
    }

}

?>