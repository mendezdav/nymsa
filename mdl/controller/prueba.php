<?php

import('mdl.view.prueba');
import('mdl.model.prueba');

class pruebaController extends controller {

    public function listar() {
        $this->view->listar();
    }

    public function agregar() {
        if (isset($_POST)):
            $this->model->get(0);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/nymsa/prueba/listar?exito=1');
        else:
            HttpHandler::redirect('/nymsa/prueba/listar');
        endif;
    }

    public function loaddata() {
        header('Content-type:text/javascript;charset=UTF-8');
        $json = json_decode(stripslashes($_POST["_gt_json"]));
        $pageNo = $json->{'pageInfo'}->{'pageNum'};
        $pageSize = 10; //10 rows per page
        //to get how many records totally.
        $sql = "select count(*) as cnt from prueba";
        $handle = mysql_query($sql);
        $row = mysql_fetch_object($handle);
        $totalRec = $row->cnt;

        //make sure pageNo is inbound
        if ($pageNo < 1 || $pageNo > ceil(($totalRec / $pageSize))) {
            $pageNo = 1;
        }

        if ($json->{'action'} == 'load') {
            $sql = "select * from prueba limit " . ($pageNo - 1) * $pageSize . ", " . $pageSize;
            $handle = mysql_query($sql);
            $retArray = array();
            while ($row = mysql_fetch_object($handle)) {
                $retArray[] = $row;
            }
            $data = json_encode($retArray);
            $ret = "{data:" . $data . ",\n";
            $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
            $ret .= "recordType : 'object'}";
            echo $ret;
        }
    }

    public function update() {
        header('Content-type:text/javascript;charset=UTF-8');
        $json = json_decode(stripslashes($_POST["_gt_json"]));
        if ($json->{'action'} == 'save') {
            $sql = "";
            $params = array();
            $errors = "";

            //deal with those deleted
            $deletedRecords = $json->{'deletedRecords'};
            foreach ($deletedRecords as $value) {
                $this->model->delete($value->id);
            }

            //deal with those updated
            $sql = "";
            $updatedRecords = $json->{'updatedRecords'};
            foreach ($updatedRecords as $value) {
                $data = get_object_vars($value);
                $this->model->get($data['id']);
                $this->model->change_status($data);
                $this->model->save();
            }



            //deal with those inserted
            $sql = "";
            $insertedRecords = $json->{'insertedRecords'};
            foreach ($insertedRecords as $value) {
                $data = get_object_vars($value);
                $this->model->get($data['id']);
                $this->model->change_status($data);
                $this->model->save();
            }


            $ret = "{success : true,exception:''}";
            echo $ret;
        }
    }

    public function pruebaList() {
        //This is a php file for data feeding
        import('scripts.pruebaDAO');
        //To create grid exporting instant.
        $gridHandler = new GridServerHandler();
        $type = getParameter('exportType');
        if ($type == 'pdf') {
            // to use html2pdf to export pdf
            // param1 : Orientation. 'P' for Portrait , 'L' for Landscape
            // param2 : Paper size. Could be A3, A4, A5, LETTER, LEGAL
            // param3 : Relative picture path to this php file
            $header = "<h1>Negocios y Mas s.a de c.v</h1>";
            $header .= "<p><br/></p> <h3>Reporte de prueba</h3>";
            $header .= "<hr/><p><br/></p>";
            $gridHandler->exportPDF('P', 'A4', '', $header);
        } else {
            //to get the data from data base. // 
            $data1 = getPruebaData();

            if ($type == 'xml') {
                //exporting to xml
                $gridHandler->exportXML($data1);
            } else if ($type == 'xls') {
                //exporting to xls
                $gridHandler->exportXLS($data1);
            } else if ($type == 'csv') {
                //exporting to csv
                $gridHandler->exportCSV($data1);
            } else {
                $data1 = getPruebaData();
                //for grid presentation
                $gridHandler->setData($data1);
                $gridHandler->setTotalRowNum(count($data1));
                $gridHandler->printLoadResponseText();
            }
        }
    }

}

?>