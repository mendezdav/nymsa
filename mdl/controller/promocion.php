<?php

import('mdl.model.promocion');
import('mdl.view.promocion');

class promocionController extends controller {

    public function guardar() {
        if (isset($_POST) && !empty($_POST)) {
            $id = addslashes($_POST['id']);
            $this->model->get($id);
            $data = $_POST;
            $data['creado_en'] = date("Y-m-d");
            $data['creado_por'] = Session::singleton()->getUser();
            $this->model->change_status($data);
            $fecha_inicio = explode('/', $_POST['inicio']);
            $fecha_fin = explode('/', $_POST['fin']);
            $f1 = checkdate($fecha_inicio[1], $fecha_inicio[2], $fecha_inicio[0]);
            $f2 = checkdate($fecha_fin[1], $fecha_fin[2], $fecha_fin[0]);
            $temp = $fecha_inicio[0];
            $fecha_inicio[0] = $fecha_inicio[2];
            $fecha_inicio[2] = $temp;
            $temp = $fecha_fin[0];
            $fecha_fin[0] = $fecha_fin[2];
            $fecha_fin[2] = $temp;
            $fecha_inicio = implode('/', $fecha_inicio);
            $fecha_fin = implode('/', $fecha_fin);
            if ($f1 && $f2) {
                if (compararFechas($fecha_fin, $fecha_inicio) > 0) {
                    $this->model->save();
                    HttpHandler::redirect('/nymsa/inventario/promociones?success=200');
                } else {
                    HttpHandler::redirect('/nymsa/inventario/promociones?errno=301');
                }
            } else {
                HttpHandler::redirect('/nymsa/inventario/promociones?errno=300');
            }
        } else {
            echo json_encode(array('Error' => 'llamada incorrecta al recurso'));
        }
    }

    public function agregar_producto() {
        $retArray = array();
        $retArray['error'] = true;

        $filtros = $_POST['filtro'];
        $temp = explode(',', $filtros);
        $filtros = array();
        foreach ($temp as $parts) {
            $tt = explode(':', $parts);
            $filtros[$tt[0]] = $tt[1];
        }

        /* CADENA DE CONDICION */
        //*/
        $fin = " WHERE ";
        $keys = array_keys($filtros);
        $values = array_values($filtros);
        $str_ct = implode(' = \'$\' ? ', $keys);
        $str_ct.= ' = \'$\' ';
        $art = explode('?', $str_ct);
        for ($i = 0; $i < count($art); $i++):
            $art[$i] = str_replace('$', $values[$i], $art[$i]);
        endfor;
        $fin .= implode(' AND ', $art);

        $query = "SELECT * FROM estado_bodega $fin AND stock > 0";
        data_model()->executeQuery($query);
        if (data_model()->getNumRows() > 0) {
            $retArray['error'] = false;
        }

        echo json_encode($retArray);
    }

}

?>