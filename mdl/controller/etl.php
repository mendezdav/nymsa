<?php

/**
 * 	Controlador para tareas de migracion de datos
 * 
 */
import('mdl.model.etl');

class etlController extends controller {

    private $db_origen  = "dbo";
    
    private $db_destino = "db_inventario";

    private $delimiter = ";";

    /**
     * $rootPath ruta hacia archivos con reglas de migracion de datos
     * @var string
     */
    private $rootPath = "D:\Nymsa\Migracion\NYMSA MIG\wtffiles\\";

    /**
     * $dataPath ruta hacia las fuentes de datos en formato csv
     * @var string
     */
    private $dataPath = "D:\inventario\\";

    /**
     * $logPath ruta hacia archivo log para registrar errores
     * @var string
     */
    private $logPath = APP_PATH;

    /**
     * __construct sobrecarga del constructor para evitar carga inecesaria de la vista
     */
    public function __construct() {
        
        $this->model = Helper::get_model($this); # load respective module
    }
	
	public function invertir_fecha($string_fecha){
		
		$fecha_inicial = explode("/", $string_fecha);
		$nueva_fecha   = array();
		if(count($fecha_inicial)==3){
			$nueva_fecha[0] = $fecha_inicial[2];
			$nueva_fecha[1] = $fecha_inicial[1];
			$nueva_fecha[2] = $fecha_inicial[0];
			
			return implode("/", $nueva_fecha);
		}else{
			return $string_fecha;
		}
	}

    /**
     * UTILIDADES PARA LA MIGRACION 
     */

    /**
     * parseWtfFile permite analizar y ejecutar las reglas de migracion de datos
     * @param  string $fileDir 
     * @param  string $dataFile
     * @return null
     */
    public function parseWtfFile($fileDir, $dataFile = null) {

        set_time_limit(9999999999999999);  # evita que la operacion se detenga por exceso de tiempo limite

        $archivo = file($fileDir);   # obtiene el identificador del archivo
        $headerFlag = false;
        $entityName = "";
        $tInfo = array();

        $tInfo['entityName'] = null;
        $tInfo['tData'] = null;


        foreach ($archivo as $linea) {
            if (!$headerFlag) {
                $len = strlen($linea);
                $entityName = substr($linea, 1, $len - 4);     # obtiene el nombre de la tabla
                $headerFlag = true;
            } else {
                $chunk = explode('=>', $linea);
                $tInfo['tData'][trim($chunk[0])] = trim($chunk[1]);   # transiciones entre campos de las tablas 	
            }
        }

        $tInfo['entityName'] = $entityName;

        // conexion con la base de datos
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino);
        BM::singleton()->getObject('db')->setActiveConnection(1);

        // conexion con el modelo de datos
        $targetModel = $this->model->get_sibling($tInfo['entityName']);

        // identificador del archivo csv
        $archivo = file($dataFile);
        $headerFlag = false;
        $colRel = array();

        // por cada linea en el archivo 
        foreach ($archivo as $linea) {

            // si analiza la primera linea
            if (!$headerFlag) {
                $ct = 0;
                $headerFlag = true;
                $cols = explode($this->delimiter, $linea);  # se separan los nombres de los campos
                foreach ($cols as $col) {
                    $colName = explode(',', $col)[0]; # se obtiene el identificador de la columna
                    $colRel[trim($colName)] = $ct++; # para cada identificador se obtiene un equivalente entero	
                }
            } else {

                $vals = explode($this->delimiter, $linea);   # se separan los valores
                $targetModel->get(0);     # se prepara el modelo para recibir los datos

                foreach ($tInfo['tData'] as $org => $des) {
                    $pos = $colRel[trim($org)];
                    if ($pos) {
                        $targetModel->$des = $vals[$pos];
                    }
                }

                $targetModel->save();     # se hace un commit a la base de datos
            }
        }
    }

    public function parseWtfFile2($fileDir, $dataFile = null) {
        set_time_limit(9999999999999999);

        $archivo = file($fileDir);
        $headerFlag = false;
        $entityName = "";
        $tInfo = array();
		
        $tInfo['entityName'] = null;
        $tInfo['tData'] = null;

        foreach ($archivo as $linea) {
            if (!$headerFlag) {
				$linea = trim($linea);
                $len = strlen($linea);
                $entityName = substr($linea, 1, $len - 2);
                $headerFlag = true;
            } else {
                $chunk = explode('=>', $linea);
                $tInfo['tData'][trim($chunk[0])] = trim($chunk[1]);
            }
        }

        $tInfo['entityName'] = $entityName;

        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino);
        BM::singleton()->getObject('db')->setActiveConnection(1);

        $targetModel = $this->model->get_child($tInfo['entityName']);

        $archivo = file($dataFile);
        $headerFlag = false;
        $colRel = array();

        foreach ($archivo as $linea) {

            if (!$headerFlag) {
                $ct = 0;
                $headerFlag = true;
                $cols = explode($this->delimiter, $linea);
                foreach ($cols as $col) {
                    $colName = explode(',', $col)[0];
                    $colRel[trim($colName)] = $ct++;
                }
            } else {

                $vals = explode($this->delimiter, $linea);
                $targetModel->get(0);
                foreach ($tInfo['tData'] as $org => $des) {
                    $pos = $colRel[trim($org)];
                    if ($pos >= 0) {
                        $targetModel->$des = $vals[$pos];
                    }
                }
                $targetModel->save();
            }
        }
    }
	
	public function parseccdiah($fileDir, $dataFile = null) {
        set_time_limit(9999999999999999);

        $archivo = file($fileDir);
        $headerFlag = false;
        $entityName = "";
        $tInfo = array();
		
        $tInfo['entityName'] = null;
        $tInfo['tData'] = null;

        foreach ($archivo as $linea) {
            if (!$headerFlag) {
				$linea = trim($linea);
                $len = strlen($linea);
                $entityName = substr($linea, 1, $len - 2);
                $headerFlag = true;
            } else {
                $chunk = explode('=>', $linea);
                $tInfo['tData'][trim($chunk[0])] = trim($chunk[1]);
            }
        }

        $tInfo['entityName'] = $entityName;

        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino);
        BM::singleton()->getObject('db')->setActiveConnection(1);

        $targetModel = $this->model->get_child($tInfo['entityName']);

        $archivo = file($dataFile);
        $headerFlag = false;
        $colRel = array();

        foreach ($archivo as $linea) {

            if (!$headerFlag) {
                $ct = 0;
                $headerFlag = true;
                $cols = explode($this->delimiter, $linea);
                foreach ($cols as $col) {
                    $colName = explode(',', $col)[0];
                    $colRel[trim($colName)] = $ct++;
                }
            } else {

                $vals = explode($this->delimiter, $linea);
                $targetModel->get(0);
                foreach ($tInfo['tData'] as $org => $des) {
                    $pos = $colRel[trim($org)];
                    if ($pos >= 0) {
						if($des == 'femora' || $des == 'fedoc' || $des=='vence'){
							$vals[$pos] = $this->formato_fecha($vals[$pos]);
							echo $vals[$pos];
						}
                        $targetModel->$des = $vals[$pos];
                    }
                }
                $targetModel->save();
			}
        }
    }
	
	public function parseccdiad($fileDir, $dataFile = null) {
        set_time_limit(9999999999999999);

        $archivo = file($fileDir);
        $headerFlag = false;
        $entityName = "";
        $tInfo = array();
		
        $tInfo['entityName'] = null;
        $tInfo['tData'] = null;

        foreach ($archivo as $linea) {
            if (!$headerFlag) {
				$linea = trim($linea);
                $len = strlen($linea);
                $entityName = substr($linea, 1, $len - 2);
                $headerFlag = true;
            } else {
                $chunk = explode('=>', $linea);
                $tInfo['tData'][trim($chunk[0])] = trim($chunk[1]);
            }
        }

        $tInfo['entityName'] = $entityName;

        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino);
        BM::singleton()->getObject('db')->setActiveConnection(1);

        $targetModel = $this->model->get_child($tInfo['entityName']);

        $archivo = file($dataFile);
        $headerFlag = false;
        $colRel = array();

        foreach ($archivo as $linea) {

            if (!$headerFlag) {
                $ct = 0;
                $headerFlag = true;
                $cols = explode($this->delimiter, $linea);
                foreach ($cols as $col) {
                    $colName = explode(',', $col)[0];
                    $colRel[trim($colName)] = $ct++;
                }
            } else {

                $vals = explode($this->delimiter, $linea);
                $targetModel->get(0);
                foreach ($tInfo['tData'] as $org => $des) {
                    $pos = $colRel[trim($org)];
                    if ($pos >= 0) {
						if($des == 'fedoc' || $des == 'fedoc2'){
							$vals[$pos] = $this->formato_fecha($vals[$pos]);
							echo $vals[$pos];
						}
                        $targetModel->$des = $vals[$pos];
                    }
                }
                $targetModel->save();
			}
        }
    }
	
	private function formato_fecha($fecha){
		echo $fecha;
		if("- -"!=trim($fecha)){
			$parts = explode("-", $fecha);
			if(count($parts)==3){
				if(!empty($parts[0]) && !empty($parts[1]) && !empty($parts[2])){
					$meses = array("Jan"=>1, "Feb"=>2, "Mar"=>3, "Apr"=>4, "May"=>5, "Jun"=>6, "Jul"=>7, "Aug"=>8, "Sep"=>9, "Oct"=>10, "Nov"=>11, "Dec"=>12);
					$temp  = $parts[0];
					$parts[0] = "20".$parts[2];
					$parts[2] = $temp;
					echo $parts[1];
					$parts[1] = $meses[$parts[1]];
					
					return implode("-", $parts);
				}else{
					return "0000-00-00";
				}
			}else{
				return "0000-00-00";
			}
		}else{
			return "0000-00-00";
		}
	}

    /**
     * FIN UTILIDADES PARA LA MIGRACION 
     */
    
    #################################################################################
    
    /**
     * MIGRACION 
     */

    # FUENTES SQLServer

    /**
     * Proceso para migración de bodegas
     * Fuente: SQLServer (bodegas)
     * Dependencias: 
     *                  * Empleado (o cliente) a quien pertenece la bodega virtual
     * @return null 
     */
    public function etlbodegas(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM bodegas";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("bodega");

        $this->model->reset('bodega');

        foreach ($raw_data as $row) {
            while (($row['bodega'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id              = $row['bodega'];
            $destination_mdl->nombre          = $row['nbodega'];
            $destination_mdl->encargado       = '';
            $destination_mdl->descripcion     = '';
            $destination_mdl->nombre_cli      = $row['encargado'];
            $destination_mdl->bodega_consigna = '0';

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de cajas
     * Fuente: SQLServer (cajas)
     * Dependencias:
     *                 * Empleado que maneja (opera) la caja
     * @return null
     */
    public function etlcajas(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM cajas";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("caja");
        $series          = $this->model->get_child("serie");

        $this->model->reset('caja');

        foreach ($raw_data as $row) {
            while (($row['caja'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id                  = $row['caja'];
            $destination_mdl->nombre              = $row['ncaja'];
            $destination_mdl->ultimo_pedido       = $row['ultimo_p'];
            $destination_mdl->ultimo_cambio       = $row['ultimo_c'];
            $destination_mdl->codigo_factura      = $row['serie_fc'];
            $destination_mdl->codigo_nota_credito = $row['serie_nc'];
            $destination_mdl->codigo_nota_debito  = $row['serie_nd'];
            $destination_mdl->codigo_recibo       = $row['serie_rc'];
            $destination_mdl->serie_factura       = $series->get_id('FC',$row['serie_fc']);
            $destination_mdl->serie_nota_credito  = $series->get_id('NC',$row['serie_nc']);
            $destination_mdl->serie_nota_debito   = $series->get_id('ND',$row['serie_nd']);
            $destination_mdl->serie_recibo        = $series->get_id('RC',$row['serie_rc']);
            $destination_mdl->p_cambio_bodega     = $row['cambiarbodega'];
            $destination_mdl->encargado           = $row['encargado'];
            $destination_mdl->bodega_por_defecto  = $row['bodega'];
            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los bancos donde se aceptan pagos
     * Fuente: SQLServer (casascredito)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlcasascredito(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM casascredito";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("casascredito");

        $this->model->reset('casascredito');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id         = $row['id'];
            $destination_mdl->casa       = (is_null($row['casa']))? 0: $row['casa'];
            $destination_mdl->cn_cuentac = (is_null($row['cn_cuentac']))? 0: $row['cn_cuentac'];
            $destination_mdl->cn_codaux  = (is_null($row['cn_codaux']))? 0: $row['cn_codaux'];
            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los catalogos de la empresa
     * Fuente: SQLServer (catalogos)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlcatalogos(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM catalogos";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("catalogo");

        $this->model->reset('catalogo');

        foreach ($raw_data as $row) {
            while (($row['catalogo'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id           = $row['catalogo'];
            $destination_mdl->inicio       = implode('/', array_reverse(explode('/', $row['inicia'])));
            $destination_mdl->final        = implode('/', array_reverse(explode('/', $row['finaliza'])));
            $destination_mdl->descripcion  = '';
            $destination_mdl->nombre       = $row['ncatalogo'];
            $destination_mdl->tipocatalogo = $row['tipocatalogo'];
            $destination_mdl->paginas      = $row['paginas'];

            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los agentes de la empresa
     * Fuente: SQLServer (ccage)
     * Dependencias: DESCONOCIDA
     * @return null
     */
    public function etlccage(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM ccage";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("agente");

        $this->model->reset('agente');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $data['cage'] = $destination_mdl->last_insert_id();

                $ccage = $data['cage'];
                $id    = $row['id']; 

                if(!BUSINESS_MANAGER)
                    debugHandler::setAssert("$ccage < $id", "Error de identificador");

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id       = $row['id'];
            $destination_mdl->cage     = $row['cage'];
            $destination_mdl->pcom     = $row['pcom'];
            $destination_mdl->especial = $row['especial'];
            $destination_mdl->nombre   = $row['nage'];
            $destination_mdl->force_save();
        }   
    }

    /**
     * Proceso para migracion de los paises donde tiene operacion la empresa
     * Fuente: SQLServer (paises)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlpaises(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM paises";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("paises");

        $this->model->reset('paises');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = $row['id'];
            $destination_mdl->nombre = $row['npais'];
            $destination_mdl->region = $row['region'];

            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los departamentos/estados por pais
     * Fuente: SQLServer (ccdep)
     * Dependencias: 
     *                 * posiblemente paises
     * @return null
     */
    public function etlccdep(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM ccdep";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("departamentos");

        $this->model->reset('departamentos');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = $row['id'];
            $destination_mdl->codigo = $row['cdep'];
            $destination_mdl->nombre = $row['ndep'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los colores disponibles para los productos
     * Fuente: SQLServer (colores)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlcolores(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM colores";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("color");

        $this->model->reset('color');

        foreach ($raw_data as $row) {
            while (($row['ccolor'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = $row['ccolor'];
            $destination_mdl->nombre = $row['ncolor'];

            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de las transacciones realizadas en la empresa
     * Fuente: SQLServer (ctrans)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlctrans(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM ctrans";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("transacciones");

        $this->model->reset('transacciones');

        foreach ($raw_data as $row) {

            $destination_mdl->setVirtualId('cod');
            $destination_mdl->get(0);
            $destination_mdl->cod    = $row['codtra'];
            $destination_mdl->nombre = $row['ntra'];
            $destination_mdl->tipo   = $row['tipo'];
            $destination_mdl->ultimo = $row['ultimo'];
            $destination_mdl->grupo  = $row['grupo'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los tipos de descuentos permitidos en la empresa
     * Fuente: SQLServer (dsctostipos)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etldsctostipos(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM dsctostipos";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("tipodescuento");

        $this->model->reset('tipodescuento');

        foreach ($raw_data as $row) {

            while (($row['tipo'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = $row['tipo'];
            $destination_mdl->nombre = $row['ntipo'];
            $destination_mdl->modificable = $row['modificable'];

            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los generos para los cuales se vende
     * Fuente: SQLServer (generos)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlgeneros(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM generos";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("genero");

        $this->model->reset('genero');

        foreach ($raw_data as $row) {

            while (($row['genero'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = (is_null($row['genero']))?'':$row['genero'];
            $destination_mdl->nombre = (is_null($row['ngenero']))?'':$row['ngenero'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los grupos para relacionar productos
     * Fuente: SQLServer (grupos)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlgrupos(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM grupos";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("grupo");

        $this->model->reset('grupo');

        foreach ($raw_data as $row) {

            while (($row['grupo'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = (is_null($row['grupo']))?'':$row['grupo'];
            $destination_mdl->nombre = (is_null($row['ngrupo']))?'':$row['ngrupo'];
            //$destination_mdl->producto = $row['producto'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de las lineas de productos
     * Fuente: SQLServer (linea)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etllineas(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM lineas";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("linea");

        $this->model->reset('linea');

        foreach ($raw_data as $row) {

            while (($row['linea'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = (is_null($row['linea']))?'':$row['linea'];
            $destination_mdl->nombre = (is_null($row['nlinea']))?'':$row['nlinea'];
            $destination_mdl->producto = $row['producto'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de las marcas de productos
     * Fuente: SQLServer (marcas)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlmarcas(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM marcas";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("marca");

        $this->model->reset('marca');

        foreach ($raw_data as $row) {

            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = $row['id'];
            $destination_mdl->nombre = (is_null($row['nmarca']))?'':$row['nmarca'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los materiales de productos
     * Fuente: SQLServer (materiales)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlmateriales(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM materiales";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("material");

        $this->model->reset('material');

        foreach ($raw_data as $row) {

            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = (is_null($row['id']))?'':$row['id'];
            $destination_mdl->nombre = (is_null($row['nmaterial']))?'':$row['nmaterial'];
            //$destination_mdl->producto = $row['producto'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de las propiedades de productos
     * Fuente: SQLServer (propiedades)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlpropiedad(){
        // no se ocupan por el momento
        // permanece controlado por la aplicacion
        // sera migrado si llega a ser necesario o exigido en algun momento
    }

    /**
     * Proceso para migracion de los proveedores de la empresa
     * Fuente: SQLServer (proveedores)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlproveedores(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM proveedores";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("proveedor");

        $this->model->reset('proveedor');

        foreach ($raw_data as $row) {

            while (($row['codpro'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id                  = $row['codpro'];
            $destination_mdl->nombre              = $row['nombre'];
            $destination_mdl->direccion           = $row['direccion'];
            $destination_mdl->telefono            = $row['telefono'];
            $destination_mdl->fax                 = $row['fax'];
            $destination_mdl->correo              = $row['email'];
            $destination_mdl->nombre_contacto     = $row['contacto'];
            $destination_mdl->telefono_contacto   = $row['contacto_telefono'];
            $destination_mdl->correo_contacto     = $row['contacto_email'];
            $destination_mdl->dias_credito        = $row['diascredito'];
            $destination_mdl->dias_entrega        = $row['diasentrega'];
            $destination_mdl->pais_origen         = $row['pais'];
            $destination_mdl->margen_usual        = $row['margen'];
            $destination_mdl->nit                 = $row['nit'];
            $destination_mdl->nacionalidad        = $row['nacionalidad'];
            $destination_mdl->descuento_aplicable = $row['pdes'];

            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los tipos de series para facturas
     * Fuente: SQLServer (setiesTipo)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlseriestipo(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM seriestipo";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("seriestipo");

        $this->model->reset('seriestipo');

        foreach ($raw_data as $row) {
            

            $destination_mdl->get(0);
            $destination_mdl->id              = $row['tipo'];
            $destination_mdl->nombre          = $row['ntipo'];
            $destination_mdl->force_save();
        }        
    }

    /**
     * Proceso para migracion de los tipos de suelas para productos
     * Fuente: SQLServer (suelas)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlsuelas(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM suelas";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("suela");

        $this->model->reset('suela');

        foreach ($raw_data as $row) {

            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = (is_null($row['id']))?'':$row['id'];
            $destination_mdl->nombre = (is_null($row['nsuela']))?'':$row['nsuela'];
            //$destination_mdl->producto = $row['producto'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los tipos de tacones para productos
     * Fuente: SQLServer (tacones)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etltacones(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM tacones";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("tacon");

        $this->model->reset('tacon');

        foreach ($raw_data as $row) {

            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id     = (is_null($row['id']))?'':$row['id'];
            $destination_mdl->nombre = (is_null($row['ntacon']))?'':$row['ntacon'];
            //$destination_mdl->producto = $row['producto'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los tipos de transporte de los diferentes canales de distribucion
     * Fuente: SQLServer (transportes)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etltransportes(){
         // no se va a migrar porque no se esta usando
        // si se dan indicaciones posteriores sera migrado
    }

    /**
     * Proceso para migracion de las series de facturas
     * Fuente: SQLServer (series)
     * Dependencias: 
     *                 * seriesTipo
     * @return null
     */
    public function etlseries(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM series";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("serie");

        $this->model->reset('serie');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id               = $row['id'];
            $destination_mdl->tipo             = $row['tipo'];
            $destination_mdl->serie            = $row['serie'];
            $destination_mdl->descripcion      = '';
            $destination_mdl->resolucion       = $row['resolucion'];
            $destination_mdl->fecha_resolucion = $row['fecha_r'];
            $destination_mdl->del              = $row['del'];
            $destination_mdl->al               = $row['al'];
            $destination_mdl->ultimo_utilizado = $row['ultimo'];
            $destination_mdl->lineas           = $row['lineas'];
            $destination_mdl->fiscal           = $row['fiscal'];
            $destination_mdl->afectaiva        = $row['afectaiva'];

            $destination_mdl->save();
        }
    }

    /**
     * Proceso para migracion de los comunicados emitidos a los clientes
     * Fuente: SQLServer (cccartas)
     * Dependencias: 
     *                 * probablemente redes
     * @return null
     */
    public function etlcccartas(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM cccartas";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("cartas");

        $this->model->reset('cartas');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id         = $row['id'];
            $destination_mdl->codcli     = $row['codcli'];
            $destination_mdl->tipocarta  = $row['tipocarta'];
            $destination_mdl->foperado   = $row['feoperado'];
            $destination_mdl->operadopor = $row['operadopor'];
            $destination_mdl->saldo      = $row['saldo'];

            $destination_mdl->save();
        }           
    }

    /**
     * Proceso para migracion de las cabeceras de ordenes de compras
     * Fuente: SQLServer (comprash)
     * Dependencias: 
     *                 * probablemente NINGUNO
     * @return null
     */
    public function etlcomprash(){
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM comprash";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("orden_compra");

        $this->model->reset('orden_compra');

        foreach ($raw_data as $row) {
            while (($row['numero'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id             = $row['numero'];
            $destination_mdl->estado         = $row['estatus'];
            $destination_mdl->tipo           = '';
            $destination_mdl->fecha          = $row['fecha'];
            $destination_mdl->fecha_entrega  = $row['fechaentrega'];
            $destination_mdl->proveedor      = $row['codpro'];
            $destination_mdl->total          = 0;
            $destination_mdl->total_costo    = $row['totalcosto'];
            $destination_mdl->linea          = $row['linea'];
            $destination_mdl->concepto       = $row['concepto'];
            $destination_mdl->observaciones  = $row['observaciones'];
            $destination_mdl->cc             = $row['cc'];
            $destination_mdl->parciales      = $row['parciales'];
            $destination_mdl->tipo_int       = $row['tipo'];
            $destination_mdl->total2         = $row['total'];
            $destination_mdl->total_ingresos = $row['totalingresos'];
            $destination_mdl->saldo          = $row['saldo'];
            $destination_mdl->cancelados     = $row['cancelados'];
            $destination_mdl->catalogo       = (is_null($row['catalogo']))? 0:$row['catalogo'];
            $destination_mdl->editable       = '0';
        
            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de los detalles de ordenes de compras
     * Fuente: SQLServer (comprasd)
     * Dependencias: 
     *                 * comprash
     * @return null
     */
    public function etlcomprasd(){
        set_time_limit(9999999999999999);
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
               
        $raw_data        = array();
        $destination_mdl = null;

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $extraction_query = "SELECT * FROM comprasd";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){

            $raw_data[] = $temp; 
        }

        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("detalle_orden_compra");

        $this->model->reset('detalle_orden_compra');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }

                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }


            list($linea, $estilo, $color, $talla) = $this->model->get_child('estilo')->findData($row['id_prod']); 

            $id_prod = $row['id_prod'];
            

            //debugHandler::setAssert(" $linea != -1 && $talla != -1 && $color != -1", "No se encuentra el producto: $id_prod");

            $destination_mdl->get(0);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->id         = $row['id'];
            $destination_mdl->estilo     = $estilo;
            $destination_mdl->linea      = $linea;
            $destination_mdl->talla      = $talla;
            $destination_mdl->color      = $color;
            $destination_mdl->cantidad   = $row['cantidad'];
            $destination_mdl->costo      = $row['costo'];
            $destination_mdl->id_orden   = $row['numero'];
            $destination_mdl->recibidos  = 0;
            $destination_mdl->bodega     = $row['bodega'];
            $destination_mdl->importe    = $row['importe'];
            $destination_mdl->propiedad  = $row['propiedad'];
            $destination_mdl->ingresos   = $row['ingresos'];
            $destination_mdl->cancelados = $row['cancelados'];
            $destination_mdl->saldo      = $row['saldo'];
            $destination_mdl->catalogo   = (is_null($row['catalogo']))? 0:$row['catalogo'];
            
            $destination_mdl->save();
        }   
    }

    /**
     * Proceso para migracion de las cabeceras de los pedidos realizados en caja
     * Fuente: SQLServer (pedidosh)
     * Dependencias: 
     *                 * Cliente que realiza el pedido (redes)
     * @return null
     */
    public function etlpedidosh(){
	    set_time_limit(99999999999999999999999999999999);
	    ini_set("memory_limit","2048M");
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;
        $limit = 500;
        $count = 0;
        $refped = $this->model->get_child('caja_pedido_referencia');

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $nped = 0;
    	
		$extraction_query = "SELECT noped,caja,codtra,serie,noped,feped,fc_serie,fc_nodoc,fc_fedoc,nofac,fefac,estatus,codcli,formapago,efectivo FROM pedidosh";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){
            $raw_data[] = $temp;
        }
	
        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_sibling("factura");

        $this->model->reset('factura');
        $this->model->reset('caja_pedido_referencia');

         foreach ($raw_data as $row) {
    
			$destination_mdl->get(0);
			$destination_mdl->caja            = is_null($row['caja'])?0:$row['caja'];        
			$destination_mdl->codtra          = is_null($row['codtra'])?0:$row['codtra'];           
			$destination_mdl->serie           = is_null($row['serie'])?0:$row['serie'];           
			$destination_mdl->feped           = is_null($row['feped'])?0:$row['feped'];         
			$destination_mdl->fc_serie        = is_null($row['fc_serie'])?0:$row['fc_serie'];       
			$destination_mdl->fc_nodoc        = is_null($row['fc_nodoc'])?0:$row['fc_nodoc'];      
			$destination_mdl->fc_fedoc        = is_null($row['fc_fedoc'])?0:$row['fc_fedoc'];       
			$destination_mdl->nofac           = is_null($row['nofac'])?0:$row['nofac'];      
			$destination_mdl->fecha           = is_null($row['fefac'])?0:$this->invertir_fecha($row['fefac']);  
			$destination_mdl->estatus         = is_null($row['estatus'])?0:$row['estatus'];       
			$destination_mdl->id_cliente      = is_null($row['codcli'])?0:$row['codcli']; 
			$destination_mdl->formapago       = is_null($row['formapago'])?0:$row['formapago'];     
			$destination_mdl->efectivo        = is_null($row['efectivo'])?0:$row['efectivo'];
			$destination_mdl->save();

			$refped->get(0);
			$refped->set_attr('caja', $row['caja']);
			$refped->set_attr('pedido', $row['noped']);
			$refped->set_attr('referencia', $destination_mdl->last_insert_id()); 
			$refped->save();

			$count++;
			if($count>$limit) break;
         }
    }

    public function etlpedidosh2(){
        set_time_limit(99999999999999999999999999999999);
        ini_set("memory_limit","2048M");
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;
        $limit = 500;
        $count = 0;
        $refped = $this->model->get_child('caja_pedido_referencia');

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $nped = 0;
        
        $extraction_query = "SELECT noped,caja,cheque,tarjeta,deposito,nota,financiado,credito,financiera,codcli2,venta_b,descuento,flete,enganche,saldofinanciar,plazo,factor,recargos FROM pedidosh";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){
            $raw_data[] = $temp;
        }
    
        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_sibling("factura");

        //$this->model->reset('factura');

        foreach ($raw_data as $row) {
            while (($row['noped'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }
        
                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }
            
            $ref = $refped->obtener_referencia($row['caja'], $row['noped']);
            $destination_mdl->setVirtualId('id_factura');
            $destination_mdl->get($ref);
            $destination_mdl->cheque          = $row['cheque'];
            $destination_mdl->tarjeta         = $row['tarjeta'];
            $destination_mdl->deposito        = $row['deposito'];
            $destination_mdl->nota            = $row['nota'];      
            $destination_mdl->financiado      = $row['financiado'];
            $destination_mdl->tipo            = $row['credito'];
            $destination_mdl->financiera      = $row['financiera'];   
            $destination_mdl->codcli2         = $row['codcli2'];     
            $destination_mdl->subtotal        = $row['venta_b'];   
            $destination_mdl->descuento       = $row['descuento'];
            $destination_mdl->flete           = $row['flete'];      
            $destination_mdl->enganche        = $row['enganche'];   
            $destination_mdl->saldofinanciar  = $row['saldofinanciar'];
            $destination_mdl->plazo           = $row['plazo'];  
            $destination_mdl->factor          = $row['factor'];  
            $destination_mdl->recargos        = $row['recargos'];  
            $destination_mdl->save();

            $count++;
            if($count>$limit) break;
        }
    }

    public function etlpedidosh3(){
        set_time_limit(99999999999999999999999999999999);
        ini_set("memory_limit","2048M");
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;
        $limit = 500;
        $count = 0;
        $refped = $this->model->get_child('caja_pedido_referencia');

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $nped = 0;
        
        $extraction_query = "SELECT noped,caja,venta_n,cuota,ultimacuota,facturado,rechazado,anulado,observaciones,monto,iva,total,venta,servicio,exportacion,ch_numero,ch_banco FROM pedidosh";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){
            $raw_data[] = $temp;
        }
    
        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_sibling("factura");

        //$this->model->reset('factura');

        foreach ($raw_data as $row) {
            while (($row['noped'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }
        
                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }
        
            $ref = $refped->obtener_referencia($row['caja'], $row['noped']);
            $destination_mdl->setVirtualId('id_factura');
            $destination_mdl->get($ref);
            $destination_mdl->total           = $row['venta_n'];
            $destination_mdl->cuota           = $row['cuota'];
            $destination_mdl->ultimacuota     = $row['ultimacuota'];  
            $destination_mdl->facturado       = $row['facturado']; 
            $destination_mdl->rechazado       = $row['rechazado'];
            $destination_mdl->anulado         = $row['anulado'];
            $destination_mdl->observaciones   = $row['observaciones'];
            $destination_mdl->monto           = $row['monto'];
            $destination_mdl->iva             = $row['iva'];
            $destination_mdl->total           = $row['total'];
            $destination_mdl->venta           = $row['venta'];
            $destination_mdl->servicio        = $row['servicio'];
            $destination_mdl->exportacion     = $row['exportacion'];
            $destination_mdl->ch_numero       = $row['ch_numero']; 
            $destination_mdl->ch_banco        = $row['ch_banco'];  
            $destination_mdl->save();

            $count++;
            if($count>$limit) break;
        }
    }

    public function etlpedidosh4(){
        set_time_limit(99999999999999999999999999999999);
        ini_set("memory_limit","2048M");
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;
        $limit = 500;
        $count = 0;
        $refped = $this->model->get_child('caja_pedido_referencia');


        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $nped = 0;
        
        $extraction_query = "SELECT noped,caja,ch_previse,ta_numero,ta_vence,ta_casa,codven,concepto,diascred,transporte,pares,tasa,tdoc,descuentopor,ta_autorizacion,vence FROM pedidosh";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){
            $raw_data[] = $temp;
        }
    
        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_sibling("factura");

        //$this->model->reset('factura');

        foreach ($raw_data as $row) {
            while (($row['noped'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }
        
                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }
        
            $ref = $refped->obtener_referencia($row['caja'], $row['noped']);
            $destination_mdl->setVirtualId('id_factura');
            $destination_mdl->get($ref);
            $destination_mdl->ch_previse      = $row['ch_previse'];  
            $destination_mdl->ta_numero       = $row['ta_numero']; 
            $destination_mdl->ta_vence        = $row['ta_vence'];  
            $destination_mdl->ta_casa         = $row['ta_casa'];
            $destination_mdl->codven          = $row['codven'];  
            $destination_mdl->concepto        = $row['concepto'];
            $destination_mdl->diascred        = $row['diascred'];
            $destination_mdl->transporte      = $row['transporte'];
            $destination_mdl->pares           = $row['pares'];
            $destination_mdl->tasa            = $row['tasa'];
            $destination_mdl->tdoc            = $row['tdoc'];
            $destination_mdl->descuentopor    = $row['descuentopor'];
            $destination_mdl->ta_autorizacion = $row['ta_autorizacion'];
            $destination_mdl->fecha_vence     = $row['vence'];
            $destination_mdl->save();

            $count++;
            if($count>$limit) break;
        }
    }

    /**
     * Proceso para migracion de los detalles de los pedidos realizados en caja
     * Fuente: SQLServer (pedidosd)
     * Dependencias: 
     *                 * pedidosh
     * @return null
     */
    public function etlpedidosd(){
        set_time_limit(99999999999999999999999999999999);
        ini_set("memory_limit","2048M");
        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_origen); // conexion 1
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 2
        
        $raw_data        = array();
        $destination_mdl = null;
        $limit = 500;
        $count = 0;
        $refped = $this->model->get_child('caja_pedido_referencia');
        $est = $this->model->get_child('estilo');

        /* Extraccion de datos */

        BM::singleton()->getObject('db')->setActiveConnection(1);

        $nped = 0;
        
        $extraction_query = "SELECT * FROM pedidosd";
        
        data_model()->executeQuery($extraction_query);

        while($temp = data_model()->getResult()->fetch_assoc()){
            $raw_data[] = $temp;
        }
    
        /* Insercion de datos */
        BM::singleton()->getObject('db')->setActiveConnection(2);

        $destination_mdl = $this->model->get_child("detalle_factura");

        $this->model->reset('detalle_factura');

        foreach ($raw_data as $row) {
            while (($row['id'] - 1) > $destination_mdl->last_insert_id()) {
                $data   = array();
                $campos = $destination_mdl->get_fields();
                foreach ($campos as $campo) {
                    $data[$campo] = '';
                }
        
                $destination_mdl->get(0);
                $destination_mdl->change_status($data);
                $destination_mdl->save();
            }
            list($linea, $estilo, $color, $talla) = $est->findData($row['id_prod']);
            $ref = $refped->obtener_referencia($row['caja'], $row['noped']);
            $destination_mdl->setVirtualId('id');
            $destination_mdl->get(0);
            
            $destination_mdl->caja 	= $row['caja'];        
            $destination_mdl->codtra = $row['codtra'];        
            $destination_mdl->serie 	= $row['serie'];         
            $destination_mdl->costo 	= $row['costo'];         
            $destination_mdl->pordes = $row['pordes'];            
            $destination_mdl->catalogo = $row['catalogo'];    
            $destination_mdl->pagina = $row['pagina'];      
            $destination_mdl->propiedad = $row['propiedad'];     
            $destination_mdl->seriegarantia = $row['seriegarantia']; 
            $destination_mdl->id_dsvd = $row['id_dsvd'];       
            $destination_mdl->estatus = $row['estatus'];       
            $destination_mdl->descuentopor = $row['descuentopor'];
            $destination_mdl->dv_serie = $row['dv_serie'];      
            $destination_mdl->dv_nodoc = $row['dv_nodoc'];
            $destination_mdl->id = $row['id'];    
            $destination_mdl->descuento = $row['valdes'];        
            $destination_mdl->importe = $row['importe'];    
            $destination_mdl->descripcion = $row['descrip'];  
            $destination_mdl->cantidad = $row['cantidad'];      
            $destination_mdl->precio = $row['precio'];    
            $destination_mdl->id_factura = $ref;       
            $destination_mdl->bodega = $row['bodega'];  

            $destination_mdl->linea = $linea;
            $destination_mdl->estilo = $estilo;
            $destination_mdl->color = $color;
            $destination_mdl->talla = $talla;

            $destination_mdl->save();

            $count++;
            if($count>$limit) break;
        }
		
		$clean_query = "delete from detalle_factura where id_factura = 0";
		data_model()->executeQuery($clean_query);
    }

    /**
     * Proceso para migracion de los detalles de los pedidos anulados
     * Fuente: SQLServer (pedidosa)
     * Dependencias: 
     *                 * pedidosh
     * @return null
     */
    public function etlpedidosa(){

    }

    /**
     * Proceso para migracion de los descuentos
     * Fuente: SQLServer (dsctosaplicados)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etldsctosaplicados(){

    }

    /**
     * Proceso para migracion de los vales de descuentos
     * Fuente: SQLServer (dsctosvales)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etldsctosvales(){

    }

    /**
     * Proceso para migracion de los productos en demanda
     * Fuente: SQLServer (prodsendemanda)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etlprodsendemanda(){

    }

    /**
     * Proceso para migracion de las ofertas
     * Fuente: SQLServer (ofertasd)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etlofertasd(){

    }

    /**
     * Proceso para migracion de los vales de descuentos
     * Fuente: SQLServer (dsctosvales)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etlboletash(){

    }

    /**
     * DUDOSO
     * Fuente: SQLServer (boletasd)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etlboletasd(){

    }

    # FUENTES Archivos .csv

    /**
     * Proceso para migracion de la informacion basica de los clientes de la empresa
     * Fuente: .csv (redes)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlredes() {

        $this->parseWtfFile($this->rootPath . 'redes.wtf', $this->dataPath . 'redes.csv');
    }

    /**
     * Proceso para migracion de las referencias comerciales de los clientes de la empresa
     * Fuente: .csv (clientesref)
     * Dependencias: redes
     * @return null
     */
    public function etlclientesref() {

        $this->parseWtfFile2($this->rootPath . 'clientesref.wtf', $this->dataPath . 'clientesref.csv');
    }

    /**
     * Proceso para migracion de las referencias crediticias de los clientes de la empresa
     * Fuente: .csv (clientesod)
     * Dependencias: redes
     * @return null
     */
    public function etlclientesod() {

        $this->parseWtfFile($this->rootPath . 'clientesod.wtf', $this->dataPath . 'clientesod.csv');
    }

    /**
     * Proceso para migracion de los productos en inventario
     * Fuente: .csv (costos)
     * Dependencias: colores, lineas, marcas, generos, ... etc
     * @return null
     */
    public function etlcostos() {

        set_time_limit(99999999999999); # evitamos que el script exceda el tiempo de ejecucion del servidor aumentando el limite maximo

        try {

            // conectamos con la base de datos donde sera almacenada la informacion
            BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino);
            BM::singleton()->getObject('db')->setActiveConnection(1); # cambiar conexion activa
            // Asignacion de variables
            $dataFile   = $this->dataPath . 'costos.csv';     # archivo de donde sera extraida la informacion
            $archivo    = file($dataFile);        # variable que almacena el contenido del archivo
            $headerFlag = false;          # bandera de control de lectura de cabecera
            $gBuffer    = array();          # buffer para almacenar datos generales
            $dBuffer    = array();          # buffer para almacenar datos epecificos
            $lineCount  = 1;           # contados de linea
            $logFIle    = $this->logPath . "etl.log";      # archivo de log para los errores durante la transaccion
            $mdlStr     = 'tarjeta_costo';        # nombre del modelo
            $modelo     = $this->model->get_child($mdlStr);    # modelo para la manipulacion de la base de datos
            $ultimoDoc  = 0;           # ultimo documento generado
            $docModel   = $this->model->get_child('documento');   # model para documentos
            $tjtBuffer  = array();          # datos para "tarjeta_costo"
            $prodModel  = $this->model->get_child('documento_producto');
            $colorModel = $this->model->get_child('documento_color_producto');
            $tallaModel = $this->model->get_child('documento_talla_producto');
            $invModel   = $this->model->get_sibling('inventario');

            /*
             * PASO 1: CREACION DE DOCUMENTOS
             */

            // por cada linea en el archivo
            foreach ($archivo as $linea) {

                // identificar las cabeceras de las columnas
                if (!$headerFlag) {

                    $headerFlag = true;     # marcar las columnas como leidas
                    $ct = 0;     # poner contador de columna a cero
                    $cols = explode(';', $linea); # obtener los nombres de las columnas
                    // por cada columna
                    foreach ($cols as $col) {

                        // asignar a cada nombre su respectiva posicion
                        $colName = explode(',', $col)[0];
                        $colRel[trim($colName)] = $ct++;
                    }
                } else {

                    $datos      = explode(';', $linea);
                    $tarjetaPos = $colRel['TARJETA'];
                    $creadPos   = $colRel['CREADOPOR'];
                    $fechaPos   = $colRel['FECREADO'];
                    $ultimoDoc  = $datos[$tarjetaPos];      # PASO 1.1: Obtener el ultimo documento

                    $TARJETA   = trim($datos[$tarjetaPos]);
                    $CREADOPOR = trim($datos[$creadPos]);
                    $FECREADO  = trim($datos[$fechaPos]);
                    $ESTADO    = 1;
                    $MODULO    = 'INVENTARIO';

                    $gBuffer[$ultimoDoc]['id_documento']   = $TARJETA;
                    $gBuffer[$ultimoDoc]['fecha_creacion'] = $FECREADO;
                    $gBuffer[$ultimoDoc]['propietario']    = $CREADOPOR;
                    $gBuffer[$ultimoDoc]['estado']         = $ESTADO;
                    $gBuffer[$ultimoDoc]['modulo']         = $MODULO;
                }
            }


            // creacion de documentos a ser usados
            for ($i = 1; $i <= $ultimoDoc; $i++) {

                $docModel->get(0);

                $tjtBuffer[$i] = array();

                // PASO 1.2a: El documento existe
                if (isset($gBuffer[$i])) {

                    // documento
                    $docModel->id_documento   = $gBuffer[$i]['id_documento'];
                    $docModel->fecha_creacion = $gBuffer[$i]['fecha_creacion'];
                    $docModel->propietario    = $gBuffer[$i]['propietario'];
                    $docModel->estado         = $gBuffer[$i]['estado'];
                    $docModel->modulo         = $gBuffer[$i]['modulo'];
                } else {

                    // PASO 1.2b: El documento no existe
                    //documento por defecto en caso que no exista (para el correlativo)
                    $docModel->id_documento   = $i;
                    $docModel->fecha_creacion = date("Y-m-d");
                    $docModel->propietario    = "admin";
                    $docModel->estado         = 1;
                    $docModel->modulo         = 'INVENTARIO';
                }

                $docModel->save();
            }

            $headerFlag = false; // control de cabecera
            // por cada linea en el archivo
            foreach ($archivo as $linea) {

                if (!$headerFlag) {

                    $ct = 0;
                    $headerFlag = true;
                    $cols = explode(';', $linea);

                    foreach ($cols as $col) {

                        $colName = explode(',', $col)[0];
                        $colRel[trim($colName)] = $ct++;     # se mapean los nombres de las columnas
                    }
                } else {

                    $datos = explode(';', $linea);


                    // se obtienen las posiciones
                    $estiloPos = $colRel['CESTILO'];
                    $colorPos = $colRel['CCOLOR'];
                    $tarjetaPos = $colRel['TARJETA'];
                    $corr1Pos = $colRel['RUN1'];
                    $corr2Pos = $colRel['RUN2'];
                    $lineaPos = $colRel['LINEA'];
                    $codOrPos = $colRel['REFERENCIA'];
                    $generPos = $colRel['GENERO'];
                    $marcaPos = $colRel['MARCA'];
                    $taconPos = $colRel['TACON'];
                    $suelaPos = $colRel['SUELA'];
                    $descrPos = $colRel['NESTILO'];
                    $fechaPos = $colRel['FEINGRESA'];
                    $obserPos = $colRel['OBSERVACIO'];
                    $propiPos = $colRel['PROPIEDAD'];
                    $catalPos = $colRel['CATALOGO'];
                    $paginPos = $colRel['PAGINA'];
                    $creadPos = $colRel['CREADOPOR'];
                    $provePos = $colRel['CODPRO'];

                    $noDoc = trim($datos[$tarjetaPos]);
                    $bufferIndex = count($tjtBuffer[$noDoc]);

                    // se obtienen los datos y se almacenan temporalmente en un arreglo
                    $tjtBuffer[$noDoc][$bufferIndex]['LINEA']      = trim($datos[$lineaPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['CESTILO']    = trim($datos[$estiloPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['CCOLOR']     = trim($datos[$colorPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['TARJETA']    = trim($datos[$tarjetaPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['RUN1']       = str_replace(',', '.', trim($datos[$corr1Pos]));
                    $tjtBuffer[$noDoc][$bufferIndex]['RUN2']       = str_replace(',', '.', trim($datos[$corr2Pos]));
                    $tjtBuffer[$noDoc][$bufferIndex]['REFERENCIA'] = trim($datos[$codOrPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['GENERO']     = trim($datos[$generPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['MARCA']      = trim($datos[$marcaPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['TACON']      = trim($datos[$taconPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['SUELA']      = trim($datos[$suelaPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['NESTILO']    = trim($datos[$descrPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['FEINGRESA']  = trim($datos[$fechaPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['OBSERVACIO'] = trim($datos[$obserPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['PROPIEDAD']  = trim($datos[$propiPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['CATALOGO']   = trim($datos[$catalPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['PAGINA']     = trim($datos[$paginPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['CREADOPOR']  = trim($datos[$creadPos]);
                    $tjtBuffer[$noDoc][$bufferIndex]['CODPRO']     = trim($datos[$provePos]);
                }
            }

            /* Se limpian las tablas para evitar los duplicados al correr el script nuevamente */
            $this->model->reset($mdlStr);
            $this->model->reset('documento_talla_producto');
            $this->model->reset('documento_color_producto');
            $this->model->reset('documento_producto');
            $this->model->reset('talla_producto');
            $this->model->reset('color_producto');
            $this->model->reset('producto');

            for ($i = 1; $i <= $ultimoDoc; $i++) {
                foreach ($tjtBuffer[$i] as $doc) {
                    /* Insercion a tarjeta de costos */
                    $modelo->get(0);      // se prepara el modelo
                    $modelo->CESTILO    = $doc['CESTILO'];
                    $modelo->LINEA      = $doc['LINEA'];
                    $modelo->CODORIGEN  = $doc['REFERENCIA'];
                    $modelo->CCOLOR     = $doc['CCOLOR'];
                    $modelo->DESCRIP    = $doc['NESTILO'];
                    $modelo->PROVEEDOR  = $doc['CODPRO'];
                    $modelo->CATALOGO   = $doc['CATALOGO'];
                    $modelo->PAGINA     = $doc['PAGINA'];
                    $modelo->GENERO     = $doc['GENERO'];
                    $modelo->MARCA      = $doc['MARCA'];
                    $modelo->PROPIEDAD  = $doc['PROPIEDAD'];
                    $modelo->OBSERVACIO = $doc['OBSERVACIO'];
                    $modelo->FEINGRESA  = $doc['FEINGRESA'];
                    $modelo->NODOC      = $doc['TARJETA'];
                    $modelo->TACON      = $doc['TACON'];
                    $modelo->SUELA      = $doc['SUELA'];
                    $modelo->RUN1       = $doc['RUN1'];
                    $modelo->RUN2       = $doc['RUN2'];
                    $modelo->save();

                    $virtualId = 'estilo';
                    $prodModel->setVirtualId($virtualId);
                    $prodModel->get(0);

                    $prodModel->estilo           = $doc['CESTILO'];
                    $prodModel->linea            = $doc['LINEA'];
                    $prodModel->codigo_origen    = $doc['REFERENCIA'];
                    $prodModel->descripcion      = $doc['NESTILO'];
                    $prodModel->proveedor        = $doc['CODPRO'];
                    $prodModel->catalogo         = $doc['CATALOGO'];
                    $prodModel->n_pagina         = $doc['PAGINA'];
                    $prodModel->genero           = $doc['GENERO'];
                    $prodModel->marca            = $doc['MARCA'];
                    $prodModel->propiedad        = $doc['PROPIEDAD'];
                    $prodModel->observacion      = $doc['OBSERVACIO'];
                    $prodModel->fecha_ingreso    = $doc['FEINGRESA'];
                    $prodModel->nota             = '';
                    $prodModel->numero_documento = $doc['TARJETA'];
                    $prodModel->visible          = '1';
                    $prodModel->tacon            = $doc['TACON'];
                    $prodModel->suela            = $doc['SUELA'];

                    $prodModel->save();

                    $colorModel->get(0);
                    $colorModel->color_estilo_producto = $doc['CESTILO'];
                    $colorModel->color = $doc['CCOLOR'];
                    $colorModel->linea = $doc['LINEA'];
                    $colorModel->force_save();

                    $conMedios = false;
                    $t1 = $doc['RUN1'];
                    $t2 = $doc['RUN2'];

                    for ($j = $t1; $j <= $t2; $j++) {
                        if ($conMedios) {
                            for ($m = 0; $m <= 0.8; $m+=0.2) {
                                $tallaModel->get(0);
                                $tallaModel->talla_estilo_producto = $doc['CESTILO'];
                                $tallaModel->color = $doc['CCOLOR'];
                                $tallaModel->talla = $j + $m;
                                $tallaModel->linea = $doc['LINEA'];
                                $tallaModel->force_save();
                            }
                        } else {
                            $tallaModel->get(0);
                            $tallaModel->talla_estilo_producto = $doc['CESTILO'];
                            $tallaModel->color = $doc['CCOLOR'];
                            $tallaModel->talla = $j;
                            $tallaModel->linea = $doc['LINEA'];
                            $tallaModel->force_save();
                        }
                    }
                }

                $invModel->salvarCambiosDocumento($i);
            }
        } catch (Exception $e) {
            print("Error:" . $e->getMessage() . "\n");
        }
    }

    /**
     * Proceso para migracion de las cabeceras de facturas
     * Fuente: .csv (facmesh)
     * Dependencias: NINGUNA
     * @return null
     */
    public function etlfacmesh(){
        $this->parseWtfFile2($this->rootPath . 'facmesh.wtf', $this->dataPath . 'facmesh.csv');
    }

    /**
     * Proceso para migracion de los detalles de facturas
     * Fuente: .csv (facmesd)
     * Dependencias: 
     *                 * facmesh
     * @return null
     */
    public function etlfacmesd(){
        $this->parseWtfFile2($this->rootPath . 'facmesd.wtf', $this->dataPath . 'facmesd.csv');
    }

    public function etlccdiah(){
        $this->parseccdiah($this->rootPath . 'ccdiah.wtf', $this->dataPath . 'ccdiah.csv');
    }

    public function etlccdiad(){
        $this->parseccdiad($this->rootPath . 'ccdiad.wtf', $this->dataPath . 'ccdiad.csv');
    }

    /**
     * Proceso para migracion de las cabeceras de cambios de productos
     * Fuente: .csv (dsvdiah)
     * Dependencias: NINGUNO
     * @return null
     */
    public function etldsvdiah(){
        $this->parseWtfFile2($this->rootPath . 'dsvdiah.wtf', $this->dataPath . 'dsvdiah.csv');
    }

    /**
     * Proceso para migracion de los detalles de cambios de productos
     * Fuente: .csv (dsvdiad)
     * Dependencias: 
     *                 * dsvdiah
     * @return null
     */
    public function etldsvdiad(){
        $this->parseWtfFile2($this->rootPath . 'dsvdiad.wtf', $this->dataPath . 'dsvdiad.csv');
    }

    /**
     * Proceso para migracion de las cabeceras de los movimientos realizados por mes
     * Fuente: .csv (movmesh)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etlmovmesh(){

    }

    /**
     * Proceso para migracion de los detalles de los movimientos realizados por mes
     * Fuente: .csv (movmesd)
     * Dependencias: 
     *                 * movmesh
     * @return null
     */
    public function etlmovmesd(){

    }

    /**
     * Proceso para migracion de transacciones de cambios de precios y costos
     * Fuente: .csv (uppc)
     * Dependencias: NO SE SABE
     * @return null
     */
    public function etluppc(){

    }


    /* EXTRA: FORMATEO DE ESTILOS */
    public function test_parse(){
        $file_name = "estilos.csv"; 
        $file_dir  = "/media/Datos/Nymsa/Migracion/NYMSA MIG/";
        $file      = "$file_dir$file_name";
        echo "testing: $file <br/>";
        if( is_file($file) ){
            echo "test ok!!! <br/>";
        }else{
            echo "file not found <br/>";
        }

        $f = file($file);
        $c = 0;
        $b = array();

        // conexion 0 es la base de datos del sistema
        BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, $this->db_destino); // conexion 1
        BM::singleton()->getObject('db')->setActiveConnection(1);

        $temp = $this->model->get_child("estilo");

        foreach ($f as $linea) {
            
            $b       = explode(';', str_replace('"', '', $linea));
            $id_prod = $b[0];
            $linea   = $b[1];
            $estilo  = $b[3];
            $color   = $b[4];
            $talla   = $b[5];
            $costo   = $b[7];
            $precio  = $b[8];

            $temp->get(0);
            $temp->id_prod = $id_prod;
            $temp->linea   = $linea;
            $temp->estilo  = $estilo;
            $temp->color   = $color;
            $temp->talla   = $talla;
            $temp->costo   = $costo;
            $temp->precio  = $precio;
            $temp->save();
        }

    }

}

?>