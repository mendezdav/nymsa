<?php

class creditoView {

    private function load_settings() {
        import('scripts.periodos');
        $pf = "";
        $pa = "";
        list($pf, $pa) = cargar_periodos();
        page()->addEstigma("periodo_fiscal", $pf);
        page()->addEstigma("periodo_actual", $pa);
        page()->addEstigma("fecha_sistema", date('d/m/Y'));
    }

    public function test() {
        template()->buildFromTemplates('test.html');
        print page()->getContent();
    }

    public function principal($user) {
        template()->buildFromTemplates('template_nofixed.html');
        template()->addTemplateBit('content', 'credito/credito.html');
        $this->load_settings();
        page()->setTitle('Creditos');
        page()->addEstigma('TITULO', 'Creditos');
        page()->addEstigma('back_url', '/nymsa/modulo/listar');
        page()->addEstigma('username', $user);
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }

    public function anular_recibos($user) {
        template()->buildFromTemplates('template_nofixed.html');
        template()->addTemplateBit('content', 'credito/anular_recibo.html');
        $this->load_settings();
        page()->setTitle('Anular recibos');
        page()->addEstigma('TITULO', 'Anular recibos');
        page()->addEstigma('back_url', '/nymsa/credito/principal');
        page()->addEstigma('username', $user);
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }

    public function anular_nota_credito($user) {
        template()->buildFromTemplates('template_nofixed.html');
        template()->addTemplateBit('content', 'credito/anular_nota_credito.html');
        $this->load_settings();
        page()->setTitle('Anular nota de credito');
        page()->addEstigma('TITULO', 'Anular nota de credito');
        page()->addEstigma('back_url', '/nymsa/credito/principal');
        page()->addEstigma('username', $user);
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }

    public function cobros($user, $data, $ultimo_recibo) {
        template()->buildFromTemplates('template_nofixed.html');
        template()->addTemplateBit('content', 'credito/cobros.html');
        $this->load_settings();
        page()->setTitle('Cobros');
        page()->addEstigma('TITULO', 'Cobros');
        page()->addEstigma('ultimo_generado', $ultimo_recibo);
        page()->addEstigma('serie_recibo', $data['serie_recibo']);
        page()->addEstigma('codigo_recibo', $data['codigo_recibo']);
        page()->addEstigma('back_url', '/nymsa/credito/principal');
        page()->addEstigma('username', $user);
        page()->addEstigma('id_caja', $data['id']);
        page()->addEstigma('nombre_caja', $data['nombre']);
        page()->addEstigma('fecha', date("Y-m-d"));
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }

    public function notas_de_credito($user, $data) {
        template()->buildFromTemplates('template_nofixed.html');
        template()->addTemplateBit('content', 'credito/nota_credito.html');
        $this->load_settings();
        page()->setTitle('Notas de credito');
        page()->addEstigma('TITULO', 'Notas de credito');
        page()->addEstigma('ultimo_generado', $data['ultima_nota_credito']);
        page()->addEstigma('serie_nota_credito', $data['serie_nota_credito']);
        page()->addEstigma('codigo_nota_credito', $data['codigo_nota_credito']);
        page()->addEstigma('back_url', '/nymsa/credito/principal');
        page()->addEstigma('username', $user);
        page()->addEstigma('id_caja', $data['id']);
        page()->addEstigma('nombre_caja', $data['nombre']);
        page()->addEstigma('fecha', date("Y-m-d"));
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }

    public function mantenimiento($user, $cache) {
        template()->buildFromTemplates('template_nofixed.html');
        template()->addTemplateBit('content', 'credito/mantenimiento.html');
        $this->load_settings();
        page()->setTitle('Mantenimiento a creditos');
        page()->addEstigma('TITULO', 'Mantenimiento a creditos');
        page()->addEstigma('back_url', '/nymsa/credito/principal');
        page()->addEstigma('username', $user);
        page()->addEstigma('fecha', date("Y-m-d"));
        page()->addEstigma('tipo_solicitud', array('SQL', $cache[0]));
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }

}

?>