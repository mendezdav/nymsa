<?php

class pruebaView {

    public function listar() {
        template()->buildFromTemplates('template.html');
        page()->setTitle('Pagina de prueba');
        page()->addEstigma("menu", "Ac&aacute; va el men&uacute;");
        template()->addTemplateBit('content', 'prueba.html');
        template()->addTemplateBit('footer', 'footer.html');
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }

}

?>